#!/bin/sh

cat > tmp/data.yaml << EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
EOF

oc create -f tmp/data.yaml
