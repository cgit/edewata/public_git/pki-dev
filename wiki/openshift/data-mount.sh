#!/bin/sh

oc volume dc/pki-wiki --add --name=data --type=persistentVolumeClaim --claim-name=data --mount-path=/opt/app-root/data
