#/bin/sh +x

FILENAME=$1

mkdir -p tmp
mkdir -p tmp/secrets

echo "user`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 3`" > tmp/secrets/MYSQL_USER
echo "`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 16`" > tmp/secrets/MYSQL_PASSWORD

cat > tmp/mysql.env << EOF
DATABASE_SERVICE_NAME=mysql
MYSQL_DATABASE=wiki
MYSQL_USER=`cat tmp/secrets/MYSQL_USER`
MYSQL_PASSWORD=`cat tmp/secrets/MYSQL_PASSWORD`
EOF

oc new-app openshift/mysql-persistent --param-file tmp/mysql.env
