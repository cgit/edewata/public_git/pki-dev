#/bin/sh +x

mkdir -p tmp

cat > tmp/pki-wiki.env << EOF
OPENSHIFT_MYSQL_DB_HOST=mysql.dogtagpki.svc
OPENSHIFT_MYSQL_DB_PORT=3306
OPENSHIFT_MYSQL_DB_USERNAME=`cat tmp/secrets/MYSQL_USER`
OPENSHIFT_MYSQL_DB_PASSWORD=`cat tmp/secrets/MYSQL_PASSWORD`
OPENSHIFT_APP_NAME=wiki
MEDIAWIKI_SECRET_KEY=`cat MEDIAWIKI_SECRET_KEY`
MEDIAWIKI_UPGRADE_KEY=`cat MEDIAWIKI_UPGRADE_KEY`
PKI_BACKUP_DIR=`cat PKI_BACKUP_DIR`
GDRIVE_CONFIG_DIR=/opt/app-root/data/.gdrive
EOF

oc new-app \
 openshift/php:5.6~https://github.com/dogtagpki/pki-wiki.git#mediawiki-1.23 \
 --env-file=tmp/pki-wiki.env

oc expose svc/pki-wiki --hostname `cat HOSTNAME`
