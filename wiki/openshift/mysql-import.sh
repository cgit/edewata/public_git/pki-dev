#/bin/sh +x

if [ $# != 1 ]; then
    echo "usage: mysql-import.sh <file>"
    exit 1
fi

FILE=`basename $1`
DIR=`dirname $1`

POD=`oc get pods -l deploymentconfig=mysql | grep -v NAME | awk '{ print $1; }'`

oc rsync $DIR $POD:/tmp --exclude=* --include=$FILE --no-perms
oc rsh $POD /opt/rh/rh-mysql57/root/usr/bin/mysql -u root wiki -e "source /tmp/$FILE"
oc rsh $POD rm -rf /tmp/$FILE
