#/bin/sh +x

if [ $# != 1 ]; then
    echo "usage: data-import.sh <folder>"
    exit 1
fi

FOLDER=$1

POD=`oc get pods -l deploymentconfig=pki-wiki | grep -v NAME | awk '{ print $1; }'`

oc rsync $FOLDER $POD:/opt/app-root/data --no-perms
