#!/bin/sh

docker run \
 --name mysql-server \
 --privileged \
 -v $HOME/data:/mnt/data \
 -e MYSQL_ROOT_PASSWORD=Secret.123 \
 -d \
 mysql:5.5

docker exec -ti mysql-server \
 mysql -h 127.0.0.1 --password=Secret.123 -e "create database wiki"
