#!/bin/sh

docker run \
 --name pki-wiki \
 --link mysql-server:mysql \
 --privileged \
 -v $HOME/data:/opt/app-root/data \
 -p 8080:8080 \
 --rm \
 pki-wiki
