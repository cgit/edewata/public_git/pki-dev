#!/bin/sh

s2i build \
 https://github.com/dogtagpki/pki-wiki.git \
 -r mediawiki-1.23 \
 rhscl/php-56-rhel7 pki-wiki
