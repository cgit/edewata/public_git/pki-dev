#!/bin/sh -x

rm -f /tmp/ca_signing.csr
rm -r /tmp/external.crt
rm -r /tmp/cert_chain.p7b
rm -f /tmp/ca_signing.crt

rm -f /tmp/example.crt
rm -f /tmp/example2.crt
rm -f /tmp/example.p7
rm -f /tmp/example2.p7
rm -f /tmp/example.p7b
rm -f /tmp/example2.p7b
rm -f /tmp/example3.csr
rm -f /tmp/example3.crt

pkispawn -vv -f ca-level3-step1.cfg -s CA

/bin/cp -f /tmp/ca_signing.csr .
