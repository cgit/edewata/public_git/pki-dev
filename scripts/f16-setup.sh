#!/bin/sh -x

RESTEASY_VERSION=2.3.2.Final
RESTEASY_DIR=/usr/share/resteasy-jaxrs-$RESTEASY_VERSION

mkdir -p /usr/share/java/glassfish-jaxb
cd /usr/share/java/glassfish-jaxb
ln -sf $RESTEASY_DIR/lib/jaxb-impl-2.2.4.jar jaxb-impl.jar

mkdir -p /usr/share/java/resteasy
cd /usr/share/java/resteasy
ln -sf $RESTEASY_DIR/lib/jaxrs-api-$RESTEASY_VERSION.jar jaxrs-api.jar
ln -sf $RESTEASY_DIR/lib/resteasy-jaxb-provider-$RESTEASY_VERSION.jar resteasy-jaxb-provider.jar
ln -sf $RESTEASY_DIR/lib/resteasy-jaxrs-$RESTEASY_VERSION.jar resteasy-jaxrs.jar
ln -sf $RESTEASY_DIR/lib/resteasy-jettison-provider-$RESTEASY_VERSION.jar resteasy-jettison-provider.jar
ln -sf $RESTEASY_DIR/lib/resteasy-atom-provider-$RESTEASY_VERSION.jar resteasy-atom-provider.jar

cd /usr/share/java
ln -sf $RESTEASY_DIR/lib/jaxb-api-2.2.3.jar jaxb-api.jar
ln -sf $RESTEASY_DIR/lib/scannotation-1.0.3.jar scannotation.jar
