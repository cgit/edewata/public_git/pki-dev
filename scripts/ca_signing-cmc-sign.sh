#!/bin/sh

mkdir -p tmp

cat > tmp/ca_signing-cmc-request.cfg << EOF
# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default is internal).
tokenname=internal

# Nickname for agent certificate.
nickname=caadmin

# Request format: pkcs10 or crmf.
format=pkcs10

# Total number of PKCS10/CRMF requests.
numRequests=1

# Path to the PKCS10/CRMF request.
# The content must be in Base-64 encoded format.
# Multiple files are supported. They must be separated by space.
input=$PWD/tmp/ca_signing.csr

# Path for the CMC request in binary format
output=$PWD/tmp/ca_signing-cmc-request.bin
EOF

CMCRequest tmp/ca_signing-cmc-request.cfg

cat > tmp/ca_signing-cmc-submit.cfg << EOF
# PKI server host name.
host=$HOSTNAME

# PKI server port number.
port=8443

# Use secure connection.
# For secure connection with ECC, set environment variable 'export NSS_USE_DECODED_CKA_EC_POINT=1'.
secure=true

# Use client authentication.
clientmode=true

# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default: internal).
tokenname=internal

# Nickname of agent certificate.
nickname=caadmin

# CMC servlet path
#servlet=/ca/ee/ca/profileSubmitCMCFull
servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caCMCcaCert

# Path for the CMC request.
input=tmp/ca_signing-cmc-request.bin

# Path for the CMC response.
output=tmp/ca_signing-cmc-response.bin
EOF

HttpClient tmp/ca_signing-cmc-submit.cfg

CMCResponse -i tmp/ca_signing-cmc-response.bin -o tmp/ca_signing.crt
