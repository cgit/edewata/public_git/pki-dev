#!/bin/sh -x

mkdir -p tmp

cat > tmp/kra-standalone-step1.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[KRA]
pki_admin_email=kraadmin@example.com
pki_admin_name=kraadmin
pki_admin_nickname=kraadmin
pki_admin_password=Secret.123
pki_admin_uid=kraadmin

#pki_backup_keys=True
#pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=kra,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=kra

pki_security_domain_name=EXAMPLE
pki_token_password=Secret.123

pki_standalone=True
pki_external_step_two=False

pki_storage_nickname=kra_storage
pki_transport_nickname=kra_transport
pki_audit_signing_nickname=kra_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem

#pki_external_storage_csr_path=tmp/kra_storage.csr
#pki_external_transport_csr_path=tmp/kra_transport.csr
#pki_external_subsystem_csr_path=tmp/subsystem.csr
#pki_external_sslserver_csr_path=tmp/sslserver.csr
#pki_external_audit_signing_csr_path=tmp/kra_audit_signing.csr
#pki_external_admin_csr_path=tmp/kra_admin.csr

pki_storage_csr_path=tmp/kra_storage.csr
pki_transport_csr_path=tmp/kra_transport.csr
pki_subsystem_csr_path=tmp/subsystem.csr
pki_sslserver_csr_path=tmp/sslserver.csr
pki_audit_signing_csr_path=tmp/kra_audit_signing.csr
pki_admin_csr_path=tmp/kra_admin.csr
EOF

pkispawn -f tmp/kra-standalone-step1.cfg -s KRA
