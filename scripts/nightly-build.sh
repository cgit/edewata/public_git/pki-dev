#!/bin/sh -x

cd `dirname $0`;

./repo-remove.sh
./repo-create.sh

./pki-update.sh
./pki-build.sh

#./tet-update.sh
./tet-trim.sh
./tet-run.sh
