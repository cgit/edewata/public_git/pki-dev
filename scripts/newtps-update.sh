#!/bin/sh

/bin/cp ../../pki/base/server/share/webapps/pki/js/* /var/lib/pki/pki-tomcat/webapps/pki/js/
/bin/cp ../../pki/base/tps-tomcat/shared/webapps/tps/*.html /var/lib/pki/pki-tomcat/webapps/tps/
/bin/cp ../../pki/base/server/share/webapps/pki/css/* /var/lib/pki/pki-tomcat/webapps/pki/css/
/bin/cp ../../pki/base/tps-tomcat/shared/webapps/tps/js/* /var/lib/pki/pki-tomcat/webapps/tps/js/
