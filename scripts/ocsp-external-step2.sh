#!/bin/sh -x

mkdir -p tmp

CA_HOSTNAME=`cat tmp/ca.hostname`

cat > tmp/ocsp-external-step2.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[OCSP]
pki_admin_email=ocspadmin@example.com
pki_admin_name=ocspadmin
pki_admin_nickname=ocspadmin
pki_admin_password=Secret.123
pki_admin_uid=ocspadmin

#pki_backup_keys=True
#pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ocsp,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ocsp

pki_security_domain_hostname=$CA_HOSTNAME
pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_token_password=Secret.123

pki_external=True
pki_external_step_two=True

#pki_cert_chain_nickname=Root CA Signing Certificate - ROOT
#pki_cert_chain_nickname=External CA

#pki_cert_chain_path=tmp/cert_chain.p7b
#pki_cert_chain_path=tmp/external.crt

#pki_ca_signing_nickname=ca_signing

#pki_external_ca_cert_path=tmp/ca_signing.crt
#pki_ca_signing_cert_path=tmp/ca_signing.crt

pki_ocsp_signing_nickname=ocsp_signing
pki_audit_signing_nickname=ocsp_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem

#pki_external_signing_cert_path=tmp/ocsp_signing.crt
#pki_external_subsystem_cert_path=tmp/subsystem.crt
#pki_external_sslserver_cert_path=tmp/sslserver.crt
#pki_external_audit_signing_cert_path=tmp/ocsp_audit_signing.crt
#pki_external_admin_cert_path=tmp/ocsp_admin.crt

pki_ocsp_signing_cert_path=tmp/ocsp_signing.crt
pki_subsystem_cert_path=tmp/subsystem.crt
pki_sslserver_cert_path=tmp/sslserver.crt
pki_audit_signing_cert_path=tmp/ocsp_audit_signing.crt
pki_admin_cert_path=tmp/ocsp_admin.crt
EOF

pkispawn -f tmp/ocsp-external-step2.cfg -s OCSP
