#!/bin/sh

mkdir -p tmp

cat > tmp/kra_transport-cmc-request.cfg << EOF
# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default is internal).
tokenname=internal

# Nickname for agent certificate.
nickname=caadmin

# Request format: pkcs10 or crmf.
format=pkcs10

# Total number of PKCS10/CRMF requests.
numRequests=1

# Path to the PKCS10/CRMF request.
# The content must be in Base-64 encoded format.
# Multiple files are supported. They must be separated by space.
input=$PWD/tmp/kra_transport.csr

# Path for the CMC request in binary format
output=$PWD/tmp/kra_transport-cmc-request.bin
EOF

CMCRequest tmp/kra_transport-cmc-request.cfg

cat > tmp/kra_transport-cmc-submit.cfg << EOF
# PKI server host name.
host=$HOSTNAME

# PKI server port number.
port=8443

# Use secure connection.
# For secure connection with ECC, set environment variable 'export NSS_USE_DECODED_CKA_EC_POINT=1'.
secure=true

# Use client authentication.
clientmode=true

# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default: internal).
tokenname=internal

# Nickname of agent certificate.
nickname=caadmin

# CMC servlet path
#servlet=/ca/ee/ca/profileSubmitCMCFullKRAtransportCert
servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caCMCkraTransportCert

# Path for the CMC request.
input=tmp/kra_transport-cmc-request.bin

# Path for the CMC response.
output=tmp/kra_transport-cmc-response.bin
EOF

HttpClient tmp/kra_transport-cmc-submit.cfg

CMCResponse -d ~/.dogtag/nssdb -i tmp/kra_transport-cmc-response.bin -o tmp/kra_transport.crt

#BtoA tmp/kra_transport-cmc-response.bin tmp/kra_transport-cmc-response.b64
#echo "-----BEGIN PKCS7-----" > tmp/kra_transport.crt
#cat tmp/kra_transport-cmc-response.b64 >> tmp/kra_transport.crt
#echo "-----END PKCS7-----" >> tmp/kra_transport.crt
