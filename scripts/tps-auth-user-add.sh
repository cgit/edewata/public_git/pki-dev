#!/bin/sh

uid=$1

ldapadd -h $HOSTNAME -p 389 -D "cn=Directory Manager" -w Secret.123 << EOF
dn: uid=$uid,ou=people,dc=example,dc=com
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
uid: $uid
cn: Test User
sn: User
givenName: Test
userPassword: Secret.123
EOF
