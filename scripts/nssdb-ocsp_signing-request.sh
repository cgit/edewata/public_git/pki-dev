#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=OCSP Signing Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/ocsp_signing.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --extKeyUsage ocspResponder \
 --extGeneric 1.3.6.1.5.5.7.48.1.5:not-critical:/dev/null

openssl req -inform der -in nssdb/ocsp_signing.csr.der -out nssdb/ocsp_signing.csr

openssl req -text -noout -in nssdb/ocsp_signing.csr
