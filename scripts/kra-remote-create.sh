#!/bin/sh -x

mkdir -p tmp

CA_HOSTNAME=`cat tmp/ca.hostname`

#cp external.crt /tmp
#cp cert_chain.p7b /tmp

cat > tmp/kra.cfg << EOF
[DEFAULT]
#pki_instance_name=pki-tomcat
#pki_http_port=18080
#pki_https_port=18443
pki_pin=Secret.123

[Tomcat]
#pki_ajp_port=18009
#pki_tomcat_server_port=18005

[KRA]
#pki_admin_cert_file=ca_admin.cert
pki_import_admin_cert=False
pki_admin_email=kraadmin@example.com
pki_admin_name=kraadmin
pki_admin_nickname=kraadmin
pki_admin_password=Secret.123
pki_admin_uid=kraadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=kra,dc=pki,dc=example,dc=com
pki_ds_database=kra
pki_ds_password=Secret.123

pki_security_domain_hostname=$CA_HOSTNAME
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_token_password=Secret.123

#pki_server_pkcs12_path=pki-server.p12
#pki_server_pkcs12_password=Secret.123

pki_storage_nickname=kra_storage
pki_transport_nickname=kra_transport
pki_audit_signing_nickname=kra_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -vvv -f tmp/kra.cfg -s KRA
