#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=DRM Transport Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/kra_transport.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage clientAuth

openssl req -inform der -in nssdb/kra_transport.csr.der -out nssdb/kra_transport.csr
