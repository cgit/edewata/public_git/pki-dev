#!/bin/sh -x

ID=$1

mkdir -p tmp
openssl rand -out tmp/input.bin 2048

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin kra-key-archive --clientKeyID $ID --input-data tmp/input.bin

