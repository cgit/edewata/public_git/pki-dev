#!/bin/sh -x

. ./ra-include.sh

pkicreate -pki_instance_root=$INSTANCE_ROOT             \
             -pki_instance_name=$RA_INSTANCE_NAME      \
             -subsystem_type=$RA_SUBSYSTEM_TYPE        \
             -secure_port=$RA_SECURE_PORT              \
             -non_clientauth_secure_port=$RA_NON_CLIENTAUTH_SECURE_PORT \
             -unsecure_port=$RA_UNSECURE_PORT          \
             -user=$INSTANCE_USER                       \
             -group=$INSTANCE_GROUP                     \
             -redirect conf=/etc/$RA_INSTANCE_NAME     \
             -redirect logs=/var/log/$RA_INSTANCE_NAME \
             -verbose
