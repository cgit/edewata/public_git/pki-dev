#!/bin/sh -x

. ./ra-include.sh

pkiremove -pki_instance_root=$INSTANCE_ROOT    \
          -pki_instance_name=$RA_INSTANCE_NAME \
          -force                               \
          -verbose
