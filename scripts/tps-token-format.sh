#!/bin/sh

uid=$1
cuid=$2

if [ "$cuid" == "" ]; then
    #cuid=a00192030405060708c9
    #cuid=A7D05D2BA7D1AFB4E7C1
    cuid=`hexdump -v -n "10" -e '1/1 "%02x"' /dev/urandom`
fi

echo $cuid

tpsclient <<EOF
op=var_set name=ra_host value=localhost
op=var_set name=ra_port value=8080
op=var_set name=ra_uri value=/tps/tps
op=var_list

#op=token_status

op=token_set cuid=$cuid msn=01020304 app_ver=6FBBC105 key_info=0101 major_ver=0 minor_ver=0
#op=token_set cuid=$cuid app_ver=6FBBC105 key_info=0101

op=token_set auth_key=404142434445464748494a4b4c4d4e4f
op=token_set mac_key=404142434445464748494a4b4c4d4e4f
op=token_set kek_key=404142434445464748494a4b4c4d4e4f

op=token_status

op=ra_format uid=$uid pwd=Secret.123 new_pin=Secret.123 num_threads=1 extensions=tokenType=userKey

#op=token_status

op=exit
EOF
