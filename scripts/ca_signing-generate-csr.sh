#!/bin/sh

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -h internal \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=CA Signing Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -2 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 -o ca_signing.csr.der

openssl req -inform der -in ca_signing.csr.der -out ca_signing.csr

#BtoA ca_signing.csr.der ca_signing.csr.pem
#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_signing.csr
#cat ca_signing.csr.pem >> ca_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_signing.csr

rm ca_signing.csr.der
#rm ca_signing.csr.pem

openssl req -text -noout -in ca_signing.csr
