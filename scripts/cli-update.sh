#!/bin/sh -x

/bin/cp ../../pki/base/java-tools/bin/* /usr/bin/
/bin/cp ../../pki/base/common/python/pki/* /usr/lib/python2.7/site-packages/pki/
/bin/cp ../../pki/base/common/python/pki/cli/* /usr/lib/python2.7/site-packages/pki/cli/

/bin/cp ../../pki/base/server/sbin/* /usr/sbin/
/bin/cp ../../pki/base/server/scripts/restore-* /usr/share/pki/scripts/
/bin/cp ../../pki/base/server/etc/default.cfg /etc/pki/
/bin/cp ../../pki/base/server/python/pki/server/* /usr/lib/python2.7/site-packages/pki/server/
/bin/cp ../../pki/base/server/python/pki/server/deployment/* /usr/lib/python2.7/site-packages/pki/server/deployment/
/bin/cp ../../pki/base/server/python/pki/server/deployment/scriptlets/* /usr/lib/python2.7/site-packages/pki/server/deployment/scriptlets/
/bin/cp ../../pki/base/server/python/pki/server/cli/* /usr/lib/python2.7/site-packages/pki/server/cli/
