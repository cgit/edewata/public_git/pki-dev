#!/bin/sh -x

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-add tpsoperator --fullName "TPS Operator"
pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-group-member-add "TPS Operators" tpsoperator

pki -c Secret.123 client-init --force

REQUEST_ID=`pki -c Secret.123 client-cert-request uid=tpsoperator | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-cert-add tpsoperator --serial $CERT_ID
pki -c Secret.123 client-cert-import tpsoperator --serial $CERT_ID

pki -c Secret.123 client-cert-show tpsoperator --pkcs12 tpsoperator.p12 --pkcs12-password Secret.123
