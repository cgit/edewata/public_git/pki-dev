#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=Subsystem Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/subsystem.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage clientAuth,serverAuth

openssl req -inform der -in nssdb/subsystem.csr.der -out nssdb/subsystem.csr
