#!/bin/sh

INPUT=$1

#    -H "Accept: application/xml" \

SSL_DIR=~/.dogtag/pki-tomcat/ca/alias curl -E "caadmin:Secret123" \
    -H "Content-Type: application/xml" \
    -X PATCH \
    --data @$INPUT \
    https://$HOSTNAME:8443/tps/rest/config | xmllint --format -
