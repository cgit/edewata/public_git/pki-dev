#!/bin/sh

rm -rf nssdb
mkdir nssdb
echo Secret.123 > nssdb/password.txt
certutil -N -d nssdb -f nssdb/password.txt
openssl rand -out nssdb/noise.bin 2048
