#!/bin/sh

ldapadd -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: cn=pki,cn=ldbm database,cn=plugins,cn=config
objectClass: top
objectClass: nsBackendInstance
objectClass: extensibleObject
cn: pki
nsslapd-suffix: dc=pki,dc=example,dc=com
EOF

ldapadd -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: cn="dc=pki,dc=example,dc=com",cn=mapping tree,cn=config
objectClass: top
objectClass: extensibleObject
objectClass: nsMappingTree
cn: dc=pki,dc=example,dc=com
nsslapd-backend: pki
nsslapd-state: backend
EOF

ldapadd -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: dc=pki,dc=example,dc=com
objectClass: domain
dc: pki
EOF
