#!/bin/sh -x

INPUT=$1
DATABASE=$2
NICKNAME=$3
PASSWORD=$4

pk12util -i $INPUT -d $DATABASE -W $PASSWORD
certutil -M -n $NICKNAME -t u,u,u -d $DATABASE
