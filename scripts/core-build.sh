#!/bin/sh -x

PROJECT_DIR=`cd ../.. ; pwd`

BUILD_DIR=$HOME/build/pki-core
COMPOSE=$PROJECT_DIR/pki/scripts/compose_pki_core_packages

mkdir -p $BUILD_DIR
cd $BUILD_DIR

rm -rf repo
rm -rf rpmbuild
mkdir -p rpmbuild

echo $COMPOSE --work-dir $BUILD_DIR/rpmbuild hybrid_rpms

#$COMPOSE --work-dir $BUILD_DIR/rpmbuild --without-javadoc hybrid_rpms
$COMPOSE --work-dir $BUILD_DIR/rpmbuild rpms

#mkdir -p repo
#mv `find rpmbuild/RPMS -name *.rpm` repo
#createrepo repo
