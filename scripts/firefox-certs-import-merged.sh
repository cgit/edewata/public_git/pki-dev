#!/bin/sh

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

echo HOME=$home

SRC_DIR=`cd ../.. ; pwd`

FIREFOX_DIR=$home/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

CA_INSTANCE_NAME=pki-tomcat
KRA_INSTANCE_NAME=pki-tomcat
OCSP_INSTANCE_NAME=pki-tomcat
TKS_INSTANCE_NAME=pki-tomcat

CA_ADMIN_CERT_NICKNAME="PKI Administrator's idm.lab.bos.redhat.com Security Domain ID"
KRA_ADMIN_CERT_NICKNAME="PKI Administrator's idm.lab.bos.redhat.com Security Domain ID"
OCSP_ADMIN_CERT_NICKNAME="PKI Administrator's idm.lab.bos.redhat.com Security Domain ID"
TKS_ADMIN_CERT_NICKNAME="PKI Administrator's idm.lab.bos.redhat.com Security Domain ID"

CA_CERT_DIR=/var/lib/pki/$CA_INSTANCE_NAME/alias
CA_CERT_P12=$CA_CERT_DIR/ca_admin_cert.p12

KRA_CERT_DIR=/var/lib/pki/$KRA_INSTANCE_NAME/alias
KRA_CERT_P12=$KRA_CERT_DIR/ca_admin_cert.p12

OCSP_CERT_DIR=/var/lib/pki/$OCSP_INSTANCE_NAME/alias
OCSP_CERT_P12=$OCSP_CERT_DIR/ca_admin_cert.p12

TKS_CERT_DIR=/var/lib/pki/$TKS_INSTANCE_NAME/alias
TKS_CERT_P12=$TKS_CERT_DIR/ca_admin_cert.p12

################################################################################
# Importing CA certificate
################################################################################

CA_CERT_NAME="caSigningCert cert-$CA_INSTANCE_NAME CA"

echo Exporting CA certificate...
certutil -L -d $CA_CERT_DIR -n "$CA_CERT_NAME" -a > $CA_CERT_DIR/ca.pem
AtoB $CA_CERT_DIR/ca.pem $CA_CERT_DIR/ca.crt

echo Importing CA certificate...
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$CA_CERT_NAME" -i $CA_CERT_DIR/ca.pem -t CT,C,C

################################################################################
# Importing server certificate
################################################################################

SERVER_CERT_NAME="Server-Cert cert-$CA_INSTANCE_NAME"

echo Exporting server certificate...
certutil -L -d $CA_CERT_DIR -n "$SERVER_CERT_NAME" -a > $CA_CERT_DIR/server.pem
AtoB $CA_CERT_DIR/server.pem $CA_CERT_DIR/server.crt

echo Importing server certificate...
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$SERVER_CERT_NAME" -i $CA_CERT_DIR/server.pem -t CT,C,C

################################################################################
# Importing CA admin certificate
################################################################################

if [ -e $CA_CERT_P12 ]
then
    echo Importing CA admin certificate...
    pk12util -i $CA_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
    certutil -M -n "$CA_ADMIN_CERT_NICKNAME" -t u,u,u -d $FIREFOX_DIR/$PROFILE
fi

################################################################################
# Importing KRA admin certificate
################################################################################

if [ -e $KRA_CERT_P12 ]
then
    echo Importing KRA admin certificate...
    pk12util -i $KRA_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
    certutil -M -n "$KRA_ADMIN_CERT_NICKNAME" -t u,u,u -d $FIREFOX_DIR/$PROFILE
fi

################################################################################
# Importing OCSP admin certificate
################################################################################

if [ -e $OCSP_CERT_P12 ]
then
    echo Importing OCSP admin certificate...
    pk12util -i $OCSP_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
    certutil -M -n "$OCSP_ADMIN_CERT_NICKNAME" -t u,u,u -d $FIREFOX_DIR/$PROFILE
fi

################################################################################
# Importing TKS admin certificate
################################################################################

if [ -e $TKS_CERT_P12 ]
then
    echo Importing TKS admin certificate...
    pk12util -i $TKS_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
    certutil -M -n "$TKS_ADMIN_CERT_NICKNAME" -t u,u,u -d $FIREFOX_DIR/$PROFILE
fi
