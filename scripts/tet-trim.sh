#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`
TET_DIR=$SRC_DIR/tet

cd $TET_DIR/results
rm -rf `ls | head -n -5`
