#!/bin/sh -x

mkdir -p tmp

cat > tmp/ocsp.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[OCSP]
pki_admin_cert_file=/root/.dogtag/pki-tomcat/ca_admin.cert
pki_admin_email=ocspadmin@example.com
pki_admin_name=ocspadmin
pki_admin_nickname=ocspadmin
pki_admin_password=Secret.123
pki_admin_uid=ocspadmin

pki_backup_keys=True
pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ocsp,dc=pki,dc=example,dc=com
#pki_ds_database=userRoot
pki_ds_database=ocsp
#pki_ds_create_new_db=False
pki_ds_password=Secret.123

pki_clone_pkcs12_password=Secret.123

pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_token_password=Secret.123

#pki_profiles_in_ldap=False
#pki_share_db=False

pki_ocsp_signing_nickname=ocsp_signing
pki_audit_signing_nickname=ocsp_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -f tmp/ocsp.cfg -s OCSP
