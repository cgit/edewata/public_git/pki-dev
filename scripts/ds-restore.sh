#!/bin/sh

./ds-create.sh
./ds-init.sh
./ds-backend.sh

./ds-stop.sh
/bin/cp ca.ldif /tmp/ca.ldif
./ds-start.sh

ldif2db -Z pki-tomcat -n ca -i /tmp/ca.ldif
