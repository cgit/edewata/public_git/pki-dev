#!/bin/sh

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=CA Signing Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/ca_signing.csr.der \
 -k ca_signing \
 -g 2048 \
 -Z SHA256 \
 -2 \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

openssl req -inform der -in nssdb/ca_signing.csr.der -out nssdb/ca_signing.csr
