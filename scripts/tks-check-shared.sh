#!/bin/sh

INSTANCE_DIR=/var/lib/pki/tks-master
grep "internal=" $INSTANCE_DIR/conf/password.conf | sed "s/internal=//" > $INSTANCE_DIR/conf/internal.txt
tkstool -L -d $INSTANCE_DIR/alias -f $INSTANCE_DIR/conf/internal.txt
