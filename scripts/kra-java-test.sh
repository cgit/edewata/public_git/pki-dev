#!/bin/sh

SRC_DIR=`cd ../.. ; pwd`

INSTANCE_NAME=pki-tomcat
CLIENT_CERT_DIR=~/.dogtag/pki-tomcat/ca/alias

CLASSPATH=$SRC_DIR/pki/build/classes
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-cli.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-logging.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-lang.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-io.jar
CLASSPATH=$CLASSPATH:/usr/share/java/commons-codec.jar
CLASSPATH=$CLASSPATH:/usr/share/java/jakarta-commons-httpclient.jar
CLASSPATH=$CLASSPATH:/usr/lib/java/jss4.jar
CLASSPATH=$CLASSPATH:/usr/share/java/httpcomponents/httpclient.jar
CLASSPATH=$CLASSPATH:/usr/share/java/httpcomponents/httpcore.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/jaxrs-api.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-atom-provider.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-jaxrs.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-jaxb-provider.jar
CLASSPATH=$CLASSPATH:/usr/share/java/servlet.jar

java -classpath $CLASSPATH com.netscape.cms.servlet.test.DRMTest -h localhost -p 8443 -s true -d $CLIENT_CERT_DIR -w Secret123 -c caadmin
