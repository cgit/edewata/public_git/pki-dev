#!/bin/sh -x

PROJECT_DIR=`cd ../.. ; pwd`

BUILD_DIR=$HOME/build/pki-base
COMPOSE=$PROJECT_DIR/pki/scripts/compose_pki_core_packages

mkdir -p $BUILD_DIR
cd $BUILD_DIR

rm -rf rpmbuild
mkdir -p rpmbuild

$COMPOSE --work-dir $BUILD_DIR/rpmbuild --without-server --without-javadoc hybrid_rpms

rm -rf repo
mkdir -p repo
mv `find rpmbuild/RPMS -name *.rpm` repo
#createrepo repo
