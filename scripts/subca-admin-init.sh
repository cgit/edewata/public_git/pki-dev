#!/bin/sh

#ROOTCA=`cat tmp/rootca.hostname`

pki -c Secret.123 client-init --force

#pki -h $ROOTCA -c Secret.123 client-cert-import "Root CA Signing Certificate" --ca-server
pki -c Secret.123 client-cert-import --ca-cert tmp/rootca_signing.crt

#pki -h $ROOTCA -c Secret.123 client-cert-import "Subordinate CA Signing Certificate" --serial 0x7

pki-server cert-export ca_signing --cert-file tmp/subca_signing.crt
pki -c Secret.123 client-cert-import --ca-cert tmp/subca_signing.crt

pki -c Secret.123 client-cert-import \
 --pkcs12 ~/.dogtag/pki-tomcat/ca_admin_cert.p12 \
 --pkcs12-password Secret.123

#pki -c Secret.123 pkcs12-import \
# --pkcs12-file ~/.dogtag/pki-tomcat/ca_admin_cert.p12 \
# --pkcs12-password Secret.123
