#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`/pki
BUILD_DIR=~/build/pki
BUILDROOT=~/buildroot/pki

cd $BUILD_DIR

rm -rf $BUILDROOT
mkdir -p $BUILDROOT

make install \
 DESTDIR=$BUILDROOT/pki-core-10.3.0-0.2.fc22.x86_64 \
 'INSTALL=install -p'
