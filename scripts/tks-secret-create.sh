#!/bin/sh

dd if=/dev/urandom of=tks-secret-random.bin bs=1024 count=2

INSTANCE_DIR=/var/lib/pki/pki-tomcat
#INSTANCE_DIR=/var/lib/pki/tks-master
grep "internal=" $INSTANCE_DIR/conf/password.conf | sed "s/internal=//" > $INSTANCE_DIR/conf/internal.txt

#tkstool -T -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt -z tks-secret-random.bin -o tks-secret.txt
tkstool -T -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt -z tks-secret-random.bin

#tkstool -T -d /var/lib/pki/pki-tomcat/alias/ -n sharedSecret
