#!/bin/sh

/bin/cp ../../pki/base/server/share/webapps/ROOT/*    /usr/share/pki/server/webapps/ROOT/
/bin/cp ../../pki/base/server/share/webapps/pki/*    /usr/share/pki/server/webapps/pki/
/bin/cp ../../pki/base/server/share/webapps/pki/ui/*    /usr/share/pki/server/webapps/pki/ui/
/bin/cp ../../pki/base/server/share/webapps/pki/js/*    /usr/share/pki/server/webapps/pki/js/

/bin/cp ../../pki/dogtag/common-ui/shared/css/*     /usr/share/pki/common-ui/css/

/bin/cp ../../pki/base/ca/shared/webapps/ca/* /usr/share/pki/ca/webapps/ca/
