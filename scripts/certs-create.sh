#!/bin/sh -x

. ./ca-include.sh

cd ../certs

openssl genrsa -out private.key 1024

openssl req -new -key private.key -out server.csr -subj "/O=$REALM/CN=$HOSTNAME"
openssl req -new -key private.key -out testuser.csr -subj "/O=EXAMPLE-COM/UID=testuser/CN=Test User"

openssl x509 -req -days 365 -in server.csr -signkey private.key -out server.crt
openssl x509 -req -days 365 -in testuser.csr -signkey private.key -out testuser.crt
