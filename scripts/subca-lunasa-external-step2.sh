#!/bin/sh -x

mkdir -p tmp

USER=`cat user.txt`
PREFIX=$USER/

TOKEN=lunasaDEV
PASSWORD=devLuna555

cat > tmp/subca-step2.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

pki_hsm_enable=True
pki_hsm_libfile=/usr/safenet/lunaclient/lib/libCryptoki2_64.so
#pki_hsm_libfile=/usr/lib/libcklog2.so
pki_hsm_modulename=lunasa
pki_token_name=$TOKEN
pki_token_password=$PASSWORD

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ca

pki_external=True
pki_external_step_two=True

pki_security_domain_name=EXAMPLE

pki_cert_chain_nickname=${PREFIX}external
pki_external_ca_cert_chain_path=tmp/external.crt
#pki_external_ca_cert_chain_path=tmp/cert_chain.p7b

pki_ca_signing_nickname=${PREFIX}ca_signing
pki_ca_signing_token=$TOKEN
pki_external_csr_path=tmp/ca_signing.csr
pki_external_ca_cert_path=tmp/ca_signing.crt
#pki_external_ca_cert_path=tmp/ca_signing.p7b

pki_ocsp_signing_nickname=${PREFIX}ca_ocsp_signing
pki_ocsp_signing_token=$TOKEN

pki_audit_signing_nickname=${PREFIX}ca_audit_signing
pki_audit_signing_token=$TOKEN

pki_sslserver_nickname=${PREFIX}sslserver/$HOSTNAME
pki_sslserver_token=$TOKEN

pki_subsystem_nickname=${PREFIX}subsystem
pki_subsystem_token=$TOKEN
EOF

pkispawn -vvv -f tmp/subca-step2.cfg -s CA
