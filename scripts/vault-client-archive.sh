#!/bin/python

import base64
import getopt
import subprocess
import sys

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend

import pki
import pki.client
import pki.crypto
import pki.key
import pki.kra
import pki.systemcert

def usage():
    print "usage: vault-client-archive --user-id <user ID> --secret-id <secret ID> --vault-password <password> --secret <secret>"

def main(argv):

    try:
        opts, _ = getopt.getopt(argv[1:], 'hv', [
            'user-id=', 'secret-id=', 'vault-password=', 'secret=',
            'verbose', 'help'])

    except getopt.GetoptError as e:
        print 'ERROR: ' + str(e)
        usage()
        sys.exit(1)

    verbose = False

    user_id = None
    secret_id = None
    vault_password = None
    secret = None

    for o, a in opts:
        if o == '-v':
            verbose = True

        elif o == '--user-id':
            user_id = a

        elif o == '--secret-id':
            secret_id = a

        elif o == '--vault-password':
            vault_password = a

        elif o == '--secret':
            secret = a

    if user_id is None or secret_id is None or vault_password is None or secret is None:
        usage()
        sys.exit(1)

    backend = default_backend()

    # generate key from vault password
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt="0000000000000000",
        iterations=100000,
        backend=backend
    )
    vault_key = base64.b64encode(kdf.derive(vault_password))

    if verbose:
        print "Vault Key: " + vault_key

    # encrypt secret with key
    f = Fernet(vault_key)
    data = f.encrypt(secret)

    if verbose:
        print "Encrypted Secret: " + data

    # send user ID, secret ID, and encrypted secret to server 
    subprocess.check_call(['./vault-server-archive.sh', '--user-id', user_id, '--secret-id', secret_id, '--data', data])

    print "Secret archived."

if __name__ == '__main__':
    main(sys.argv)
