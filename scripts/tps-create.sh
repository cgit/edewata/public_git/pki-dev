#!/bin/sh -x

mkdir -p tmp

cat > tmp/tps.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[TPS]
pki_admin_cert_file=/root/.dogtag/pki-tomcat/ca_admin.cert
pki_admin_email=tpsadmin@example.com
pki_admin_name=tpsadmin
pki_admin_nickname=tpsadmin
pki_admin_password=Secret.123
pki_admin_uid=tpsadmin

pki_backup_password=Secret.123

pki_ds_base_dn=dc=tps,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
#pki_ds_database=userRoot
pki_ds_database=tps
#pki_ds_create_new_db=False

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123
pki_clone_pkcs12_password=Secret.123

pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_token_password=Secret.123

pki_authdb_basedn=dc=example,dc=com
pki_authdb_port=389
pki_enable_server_side_keygen=True

#pki_profiles_in_ldap=False
#pki_share_db=False

pki_audit_signing_nickname=tps_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -f tmp/tps.cfg -s TPS
