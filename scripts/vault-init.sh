#!/bin/sh

cd ~/.dogtag/pki-tomcat

# export CA admin cert
openssl pkcs12 -nodes -in ca_admin_cert.p12 -out ca_admin_cert.pem -password file:ca/password.conf

# get transport cert
certutil -L -d /var/lib/pki/pki-tomcat/alias -n "transportCert cert-pki-tomcat KRA" -a > transport.crt
certutil -A -d ca/alias -n "KRA Transport Certificte" -i transport.crt -a -t "u,u,u"
