#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

INSTANCE_NAME=pki-tomcat
CLIENT_CERT_DIR=/var/lib/pki/$INSTANCE_NAME/ca/certs
SERVER_CERT_DIR=/var/lib/pki/$INSTANCE_NAME/alias
CERT_NAME="caSigningCert cert-${INSTANCE_NAME}"

# add admin to RA agent group
pki -u caadmin -w Secret123 group-add-member "Registration Manager Agents" caadmin

# export CA cert
certutil -L -d $SERVER_CERT_DIR -n "$CERT_NAME" -a > $CLIENT_CERT_DIR/ca.pem
AtoB $CLIENT_CERT_DIR/ca.pem $CLIENT_CERT_DIR/ca.crt

# import CA cert
certutil -A -d $CLIENT_CERT_DIR -n "$CERT_NAME" -i $CLIENT_CERT_DIR/ca.pem -t CT,c,c

CLASSPATH=$SRC_DIR/pki/build/classes
CLASSPATH=$CLASSPATH:/usr/lib64/java/jss4.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-cli.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-logging.jar
CLASSPATH=$CLASSPATH:/usr/share/java/apache-commons-lang.jar
CLASSPATH=$CLASSPATH:/usr/share/java/commons-codec.jar
CLASSPATH=$CLASSPATH:/usr/share/java/httpcomponents/httpclient.jar
CLASSPATH=$CLASSPATH:/usr/share/java/httpcomponents/httpcore.jar
CLASSPATH=$CLASSPATH:/usr/share/java/jakarta-commons-httpclient.jar
CLASSPATH=$CLASSPATH:/usr/share/java/ldapjdk.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/jaxrs-api.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-atom-provider.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-jaxrs.jar
CLASSPATH=$CLASSPATH:/usr/share/java/resteasy/resteasy-jaxb-provider.jar
CLASSPATH=$CLASSPATH:/usr/share/java/servlet.jar

# run CA test
java -classpath $CLASSPATH com.netscape.cms.servlet.test.CATest -h localhost -p 8443 -s true -d $CLIENT_CERT_DIR -w Secret123 -c "caadmin"
