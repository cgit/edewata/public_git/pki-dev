#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

cd $SRC_DIR/repo

rm -rf pki-core-*.rpm
rm -rf pki-deploy-*.rpm
rm -rf pki-selinux-*.rpm
rm -rf pki-tks-*.rpm
rm -rf pki-kra-*.rpm
rm -rf pki-common-*.rpm
rm -rf pki-native-tools-*.rpm
rm -rf pki-silent-*.rpm
rm -rf pki-util-*.rpm
rm -rf pki-setup-*.rpm
rm -rf pki-ocsp-*.rpm
rm -rf pki-ca-*.rpm
rm -rf pki-java-tools-*.rpm
rm -rf pki-symkey-*.rpm
