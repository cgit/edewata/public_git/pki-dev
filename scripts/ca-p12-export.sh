#!/bin/sh -x

rm -rf ca.p12
rm -rf ca_signing.csr
rm -rf ca_ocsp_signing.csr
rm -rf sslserver.csr
rm -rf subsystem.csr
rm -rf ca_audit_signing.csr

#grep internal= /var/lib/pki/pki-tomcat/conf/password.conf | awk -F= '{print $2;}' > internal.txt
#PKCS12Export -d /var/lib/pki/pki-tomcat/alias -p internal.txt -o ca.p12 -w password.txt

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_signing.csr
#grep ca.signing.certreq /var/lib/pki/pki-tomcat/ca/conf/CS.cfg | awk -F= '{print $2;}' >> ca_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_signing.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_ocsp_signing.csr
#sed -n "/^ca.ocsp_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> ca_ocsp_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_ocsp_signing.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > sslserver.csr
#sed -n "/^ca.sslserver.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> sslserver.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> sslserver.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > subsystem.csr
#sed -n "/^ca.subsystem.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> subsystem.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> subsystem.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_audit_signing.csr
#sed -n "/^ca.audit_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> ca_audit_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_audit_signing.csr

pki-server subsystem-cert-export ca signing \
  --csr-file ca_signing.csr \
  --pkcs12-file ca.p12 \
  --pkcs12-password-file password.txt

pki-server subsystem-cert-export ca ocsp_signing \
  --append \
  --csr-file ca_ocsp_signing.csr \
  --pkcs12-file ca.p12 \
  --pkcs12-password-file password.txt

#pki-server subsystem-cert-export ca sslserver \
#  --append \
#  --csr-file sslserver.csr \
#  --pkcs12-file ca.p12 \
#  --pkcs12-password-file password.txt

pki-server subsystem-cert-export ca subsystem \
  --append \
  --csr-file subsystem.csr \
  --pkcs12-file ca.p12 \
  --pkcs12-password-file password.txt

pki-server subsystem-cert-export ca audit_signing \
  --append \
  --csr-file ca_audit_signing.csr \
  --pkcs12-file ca.p12 \
  --pkcs12-password-file password.txt

pki pkcs12-cert-find --pkcs12-file ca.p12 --pkcs12-password-file password.txt
pki pkcs12-key-find --pkcs12-file ca.p12 --pkcs12-password-file password.txt

