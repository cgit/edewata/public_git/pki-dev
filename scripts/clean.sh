#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

cd $SRC_DIR/pki

mkdir -p build
cd build

cmake\
 -DCMAKE_VERBOSE_MAKEFILE=ON\
 -DCMAKE_INSTALL_PREFIX:PATH=/usr\
 -DLIB_INSTALL_DIR:PATH=/usr/lib64\
 -DSYSCONF_INSTALL_DIR:PATH=/etc\
 -DSHARE_INSTALL_PREFIX:PATH=/usr/share\
 -DLIB_SUFFIX=64\
 ..

make clean-dist clean-cmake
