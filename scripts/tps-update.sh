#!/bin/sh

/bin/cp ../../pki/base/tps/shared/webapps/tps/*    /usr/share/pki/tps/webapps/tps/
/bin/cp ../../pki/base/tps/shared/webapps/tps/ui/* /usr/share/pki/tps/webapps/tps/ui/
/bin/cp ../../pki/base/tps/shared/webapps/tps/js/* /usr/share/pki/tps/webapps/tps/js/
/bin/cp ../../pki/base/server/share/webapps/pki/js/*      /usr/share/pki/server/webapps/pki/js/

/bin/cp ../../pki/dogtag/common-ui/shared/css/*     /usr/share/pki/common-ui/css/

#/bin/cp ../../pki/base/server/share/webapps/pki/fonts/*    /usr/share/pki/common-ui/fonts/

/bin/cp ../../pki/base/tps/shared/conf/token-states.properties     /usr/share/pki/tps/conf/
