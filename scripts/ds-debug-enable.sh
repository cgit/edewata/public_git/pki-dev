#!/bin/sh -x

ldapmodify -x -D "cn=Directory Manager" -w Secret.123 <<EOF
dn: cn=config
changetype: modify
replace: nsslapd-errorlog-level
nsslapd-errorlog-level: 16384
EOF

systemctl restart dirsrv@pki-tomcat.service
