#!/bin/sh

mkdir -p tmp

cat > tmp/external.cfg << EOF
HOME            = tmp
RANDFILE        = tmp/random.bin

####################################################################
[ ca ]
default_ca  = CA_default        # The default ca section

[ CA_default ]

default_days    = 1000          # how long to certify for
default_crl_days = 30           # how long before next CRL
default_md  = sha256            # use public key default MD
preserve    = no                # keep passed DN ordering

x509_extensions = ca_extensions # The extensions to add to the cert

email_in_dn = no                # Don't concat the email in the DN
copy_extensions = copy          # Required to copy SANs from CSR to cert

####################################################################
[ req ]
default_bits        = 4096
default_keyfile     = tmp/external.key
distinguished_name  = ca_distinguished_name
x509_extensions     = ca_extensions
string_mask         = utf8only

####################################################################
[ ca_distinguished_name ]
countryName             = Country Name (2 letter code)
countryName_default     = US

stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Maryland

localityName                = Locality Name (eg, city)
localityName_default        = Baltimore

organizationName            = Organization Name (eg, company)
organizationName_default    = Test CA, Limited

organizationalUnitName         = Organizational Unit (eg, division)
organizationalUnitName_default = Server Research Department

commonName              = Common Name (e.g. server FQDN or YOUR name)
commonName_default      = Test CA

emailAddress            = Email Address
emailAddress_default    = test@example.com

####################################################################
[ ca_extensions ]

subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always, issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, nonRepudiation, keyCertSign, cRLSign
EOF

openssl req \
 -config tmp/external.cfg \
 -newkey rsa:2048 \
 -keyout tmp/external.key \
 -nodes \
 -x509 \
 -out tmp/external.crt \
 -subj "/O=EXTERNAL/CN=External CA" \
 -days 365

openssl x509 -text -noout -in tmp/external.crt

################################################################################
# Issuing CA signing certificate

cat > tmp/ca_signing-ext.cfg << EOF
[ ca_extensions ]

subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always, issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, nonRepudiation, keyCertSign, cRLSign
EOF

openssl x509 -req \
 -CA tmp/external.crt \
 -CAkey tmp/external.key \
 -CAcreateserial \
 -in tmp/ca_signing.csr \
 -out tmp/ca_signing.crt \
 -extfile tmp/external.cfg \
 -extensions ca_extensions \
 -set_serial 1

openssl x509 -text -noout -in tmp/ca_signing.crt

################################################################################
# Exporting certificate chain

openssl crl2pkcs7 -nocrl \
 -certfile tmp/external.crt \
 -out tmp/cert_chain.p7b
