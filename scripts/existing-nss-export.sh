#!/bin/sh -x

grep internal= /var/lib/pki/pki-tomcat/conf/password.conf | awk -F= '{print $2;}' > internal.txt

tar chzvf nssdb.tar.gz -C /var/lib/pki/pki-tomcat/alias .

pki-server subsystem-cert-export ca signing --csr-file ca_signing.csr
