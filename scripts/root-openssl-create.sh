#!/bin/sh

mkdir -p tmp

cat > tmp/root.cfg << EOF
HOME            = tmp
RANDFILE        = ${ENV::HOME}/random.bin

####################################################################
[ ca ]
default_ca  = CA_default        # The default ca section

[ CA_default ]

default_days    = 1000          # how long to certify for
default_crl_days = 30           # how long before next CRL
default_md  = sha256            # use public key default MD
preserve    = no                # keep passed DN ordering

x509_extensions = ca_extensions # The extensions to add to the cert

email_in_dn = no                # Don't concat the email in the DN
copy_extensions = copy          # Required to copy SANs from CSR to cert

####################################################################
[ req ]
default_bits        = 4096
default_keyfile     = tmp/external.key
distinguished_name  = ca_distinguished_name
x509_extensions     = ca_extensions
string_mask         = utf8only

####################################################################
[ ca_distinguished_name ]
countryName             = Country Name (2 letter code)
countryName_default     = US

stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Maryland

localityName                = Locality Name (eg, city)
localityName_default        = Baltimore

organizationName            = Organization Name (eg, company)
organizationName_default    = Test CA, Limited

organizationalUnitName         = Organizational Unit (eg, division)
organizationalUnitName_default = Server Research Department

commonName              = Common Name (e.g. server FQDN or YOUR name)
commonName_default      = Test CA

emailAddress            = Email Address
emailAddress_default    = test@example.com

####################################################################
[ ca_extensions ]

subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always, issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, nonRepudiation, keyCertSign, cRLSign
EOF

openssl req \
 -config tmp/root.cfg \
 -newkey rsa:2048 \
 -keyout tmp/root.key \
 -nodes -x509 \
 -out tmp/root.crt \
 -subj "/O=ROOT/CN=Root CA Signing Certificate" \
 -days 365

openssl x509 -text -noout -in tmp/root.crt

openssl crl2pkcs7 \
 -nocrl \
 -certfile tmp/root.crt \
 -out tmp/cert_chain.p7b

echo $HOSTNAME > tmp/root.txt
