#!/bin/sh

mkdir -p tmp

PKCS10Client \
  -d ~/.dogtag/nssdb \
  -p Secret.123 \
  -a rsa \
  -l 1024 \
  -n "UID=testuser" \
  -o tmp/testuser.csr
