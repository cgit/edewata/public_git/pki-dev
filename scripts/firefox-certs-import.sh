#!/bin/sh

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

echo HOME=$home

SRC_DIR=`cd ../.. ; pwd`

FIREFOX_DIR=$home/.mozilla/firefox
#PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`
PROFILE=$HOSTNAME

INSTANCE_NAME=pki-tomcat

################################################################################
# Importing CA certificate
################################################################################

CA_CERT_NAME="ca_signing"
CA_CERT_DIR=/var/lib/pki/$INSTANCE_NAME/alias

echo Exporting CA certificate...
certutil -L -d $CA_CERT_DIR -n "$CA_CERT_NAME" -a > $CA_CERT_DIR/ca.pem
AtoB $CA_CERT_DIR/ca.pem $CA_CERT_DIR/ca.crt

echo Importing CA certificate...
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$CA_CERT_NAME" -i $CA_CERT_DIR/ca.pem -t CT,C,C

################################################################################
# Importing server certificate
################################################################################

SERVER_CERT_NAME="sslserver"

echo Exporting server certificate...
certutil -L -d $CA_CERT_DIR -n "$SERVER_CERT_NAME" -a > $CA_CERT_DIR/server.pem
AtoB $CA_CERT_DIR/server.pem $CA_CERT_DIR/server.crt

echo Importing server certificate...
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$SERVER_CERT_NAME" -i $CA_CERT_DIR/server.pem -t CT,C,C

################################################################################
# Importing CA admin certificate
################################################################################

CA_ADMIN_CERT_P12=/root/.dogtag/$INSTANCE_NAME/ca_admin_cert.p12

if [ -e $CA_ADMIN_CERT_P12 ]
then
    echo Importing CA admin certificate...
    pk12util -i $CA_ADMIN_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret.123
    certutil -M -n caadmin -t u,u,u -d $FIREFOX_DIR/$PROFILE
fi
