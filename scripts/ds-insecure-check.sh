#!/bin/sh -x

# disable anonymous access
LDAPTLS_CACERT=ds.crt \
    ldapsearch -H ldaps://$HOSTNAME:636 -x -D "cn=Directory Manager" -w Secret123 -b "cn=config" -s base\
    nsslapd-allow-anonymous-access nsslapd-minssf nsslapd-require-secure-binds
