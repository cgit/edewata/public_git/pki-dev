#!/bin/sh -x

INSTANCE_NAME=pki-tomcat
FILE=/etc/sysconfig/pki-tomcat

# semanage port -d -t http_port_t -p tcp 8000
sed 's/^\(JAVA_OPTS="-Xdebug.*\)$/#\1/' < $FILE > $FILE.tmp
mv $FILE.tmp $FILE
