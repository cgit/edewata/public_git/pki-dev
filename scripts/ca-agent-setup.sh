#!/bin/sh -x

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-user-add caagent --fullName "CA Agent"
pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-group-member-add "Certificate Manager Agents" caagent

REQUEST_ID=`pki -c Secret123 client-cert-request uid=caagent | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-user-cert-add caagent --serial $CERT_ID
pki -c Secret123 client-cert-import caagent --serial $CERT_ID

pki -c Secret123 client-cert-show caagent --pkcs12 caagent.p12 --pkcs12-password Secret123
