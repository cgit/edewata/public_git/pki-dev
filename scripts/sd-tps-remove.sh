#!/bin/sh

TPSHOST=`cat tps.host`

ldapmodify -x -D "cn=Directory Manager" -w Secret123 -c << EOF
dn: cn=$TPSHOST:8443,cn=TPSList,ou=Security Domain,dc=ca,dc=pki,dc=example,dc=com
changetype: delete
EOF
