#!/bin/sh

echo "Generating user certificate..."

# self-signed user cert
#echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
# certutil -C -x \
# -d nssdb \
# -f nssdb/password.txt \
# -m $RANDOM \
# -a -i testuser.csr \
# -o testuser.crt \
# -c "testuser" \
# -1 -2

echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a -i nssdb/testuser.csr \
 -o nssdb/testuser.crt \
 -c "CA Signing Certificate" \
 -1 -2

echo "Importing user certificate..."
# -f nssdb/password.txt \

certutil -A \
 -d nssdb \
 -n "testuser" \
 -i nssdb/testuser.crt \
 -t ""

echo "Generating PKCS #7 file..."

openssl crl2pkcs7 \
 -nocrl \
 -certfile nssdb/ca.crt \
 -certfile nssdb/testuser.crt \
 -out nssdb/cert_chain.p7b
