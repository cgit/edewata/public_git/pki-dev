#!/bin/sh

rm -rf nssdb
mkdir -p nssdb
echo Secret123 > password.internal
certutil -N -d nssdb -f password.internal
openssl rand -out noise.bin 2048
