#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

find $SRC_DIR/pki -not -path '.git' -name '*.java' \
    -exec sed -i 's/[[:blank:]]\+$//' {} \;
