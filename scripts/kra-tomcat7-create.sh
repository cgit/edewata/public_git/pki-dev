#!/bin/sh -x

cat > kra-tomcat7.cfg << EOF
[Tomcat]
tomcat_home=/usr/share/tomcat70

[KRA]
pki_admin_cert_file=/root/.dogtag/pki-tomcat/ca_admin.cert
#pki_import_admin_cert=False
#pki_import_admin_pkcs12_file=/root/.dogtag/pki-tomcat/ca_admin_cert.p12
#pki_import_admin_pkcs12_password=Secret.123
#pki_import_admin_pkcs12_nickname=caadmin

pki_admin_email=kraadmin@example.com
pki_admin_name=kraadmin
pki_admin_nickname=kraadmin
pki_admin_password=Secret.123
pki_admin_uid=kraadmin

#pki_backup_keys=True
#pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_pkcs12_password=Secret.123
#pki_client_database_purge=False

#pki_clone_pkcs12_password=Secret.123

#pki_ds_ldaps_port=636
#pki_ds_secure_connection=True
#pki_ds_secure_connection_ca_nickname=Directory Server CA certificate
#pki_ds_secure_connection_ca_pem_file=dsca.pem

pki_ds_base_dn=dc=kra,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
#pki_ds_database=userRoot
#pki_ds_database=pki
pki_ds_database=kra
#pki_ds_create_new_db=False
#pki_ds_remove_data=False

pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123
#pki_token_password=Secret.123
#pki_share_db=False

pki_storage_nickname=storage
pki_transport_nickname=transport
pki_audit_signing_nickname=kra_audit_signing
pki_ssl_server_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -vvv -f kra-tomcat7.cfg -s KRA

#/bin/cp /var/lib/pki/pki-tomcat/alias/kra_backup_keys.p12 .
