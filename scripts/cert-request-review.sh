#!/bin/sh

REQUEST_ID=$1
OUTPUT=$2

if [ "$OUTPUT" == "" ]; then
    OUTPUT=cert-request-review.xml
fi

SRC_DIR=`cd ../.. ; pwd`
INSTANCE_NAME=ca-master
CLIENT_CERT_DIR=$HOME/.dogtag/${INSTANCE_NAME}/ca/alias

# Review request as an agent
pki -d $CLIENT_CERT_DIR -w Secret123 -n caadmin cert-request-review "$REQUEST_ID" --output "$OUTPUT"
