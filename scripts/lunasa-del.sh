#!/bin/sh

verbose=
password_file=
run=1

# read the options
TEMP=`getopt -o f:nv --long help -n 'lunasa-del.sh' -- "$@"`
eval set -- "$TEMP"

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -f)
            password_file=$2
            shift 2
            ;;
        --help)
            echo "Usage: lunasa-del.sh <prefix> -f <password file> [OPTIONS]"
            echo
            echo "Options:"
            echo " -f <password file>   File containing LunaSA password."
            echo " -n                   Dry run. Do not delete objects."
            echo " -v                   Run in verbose mode."
            echo " --help               Show help message."
            exit 0
            ;;
        -n)
            run= 
            shift
            ;;
        -v)
            verbose=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Error: invalid option $1" >&2
            echo "Run lunasa-del.sh --help for help." >&2
            exit 1
            ;;
    esac
done

prefix=$1

if [ "$verbose" != "" ]
then
    echo "prefix: $prefix"
fi

if [ "$prefix" == "" ]
then
    echo "Error: missing prefix" >&2
    echo "Run lunasa-del.sh --help for help." >&2
    exit 1
fi

if [ "$verbose" != "" ]
then
    echo "password file: $password_file"
fi

if [ "$password_file" == "" ]
then
    echo "Error: missing password file" >&2
    echo "Run lunasa-del.sh --help for help." >&2
    exit 1
fi

password="`cat $password_file`"

if [ "$verbose" != "" ]
then
    echo "run: $run"
fi

echo "Searching for objects with prefix: $prefix"

/usr/safenet/lunaclient/bin/cmu list -display handle,id,label -class certificate -password "$password" | while read cert
do
    label=$(echo $cert | cut -d' ' -f3 | cut -d= -f2)

    if [[ "$label" == "$prefix"* ]]
    then

        echo "object: $label"

        id=$(echo $cert | cut -d' ' -f2 | cut -d= -f2)
        echo " - id: $id"

        certHandle=$(echo $cert | cut -d' ' -f1 | cut -d= -f2)
        echo " - certificate: $certHandle"

        if [ "$run" == "1" ]
        then
            /usr/safenet/lunaclient/bin/cmu delete -handle $certHandle -force -password $password
        fi

        publicKey=$(/usr/safenet/lunaclient/bin/cmu list -display handle -id $id -class public -password $password)
        publicKeyHandle=$(echo $publicKey | cut -d' ' -f1 | cut -d= -f2)
        echo " - public key: $publicKeyHandle"

        if [ "$run" == "1" ]
        then
            /usr/safenet/lunaclient/bin/cmu delete -handle $publicKeyHandle -force -password $password
        fi

        privateKey=$(/usr/safenet/lunaclient/bin/cmu list -display handle -id $id -class private -password $password)
        privateKeyHandle=$(echo $privateKey | cut -d' ' -f1 | cut -d= -f2)
        echo " - private key: $privateKeyHandle"

        if [ "$run" == "1" ]
        then
            /usr/safenet/lunaclient/bin/cmu delete -handle $privateKeyHandle -force -password $password
        fi

    fi

done
