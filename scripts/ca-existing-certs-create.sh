#!/bin/sh -x

mkdir -p tmp

cat > tmp/ca-existing-certs.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ca

pki_security_domain_name=EXAMPLE

pki_token_password=Secret.123

pki_existing=True

pki_ca_signing_nickname=ca_signing
pki_ca_signing_csr_path=tmp/ca_signing.csr
pki_ca_signing_cert_path=tmp/ca_signing.crt

pki_ocsp_signing_nickname=ca_ocsp_signing
pki_ocsp_signing_csr_path=tmp/ca_ocsp_signing.csr
pki_ocsp_signing_cert_path=tmp/ca_ocsp_signing.crt

pki_sslserver_nickname=sslserver
pki_sslserver_csr_path=tmp/sslserver.csr
pki_sslserver_cert_path=tmp/sslserver.crt

pki_subsystem_nickname=subsystem
pki_subsystem_csr_path=tmp/subsystem.csr
pki_subsystem_cert_path=tmp/subsystem.crt

pki_audit_signing_nickname=ca_audit_signing
pki_audit_signing_csr_path=tmp/ca_audit_signing.csr
pki_audit_signing_cert_path=tmp/ca_audit_signing.crt

#pki_serial_number_range_start=6
#pki_request_number_range_start=1
EOF

pkispawn -v -f tmp/ca-existing-certs.cfg -s CA
