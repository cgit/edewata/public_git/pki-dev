#!/bin/sh -x

mkdir -p tmp

SUBCA=`cat tmp/subca.hostname`

cat > tmp/level3ca.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ca

pki_subordinate=True

pki_issuing_ca_hostname=$SUBCA

pki_security_domain_hostname=$SUBCA
#pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_subordinate_create_new_security_domain=True
pki_subordinate_security_domain_name=LEVEL3

pki_ca_signing_nickname=${PREFIX}ca_signing
pki_ca_signing_subject_dn=cn=CA Signing Certificate,o=EXAMPLE
pki_ca_signing_token=$TOKEN

pki_ocsp_signing_nickname=${PREFIX}ca_ocsp_signing
pki_ocsp_signing_subject_dn=cn=CA OCSP Signing Certificate,o=EXAMPLE
pki_ocsp_signing_token=$TOKEN

pki_audit_signing_nickname=${PREFIX}ca_audit_signing
pki_audit_signing_subject_dn=cn=CA Audit Signing Certificate,o=EXAMPLE
pki_audit_signing_token=$TOKEN

pki_sslserver_nickname=${PREFIX}sslserver/$HOSTNAME
pki_sslserver_subject_dn=cn=$HOSTNAME,o=EXAMPLE
pki_sslserver_token=$TOKEN

pki_subsystem_nickname=${PREFIX}subsystem/$HOSTNAME
pki_subsystem_subject_dn=cn=Subsystem Certificate,o=EXAMPLE
pki_subsystem_token=$TOKEN
EOF

pkispawn -v -f tmp/level3ca.cfg -s CA

echo $HOSTNAME > tmp/level3ca.hostname

