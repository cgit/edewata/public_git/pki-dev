#!/bin/sh -x

mkdir -p tmp

MASTER=`cat tmp/master.txt`

cat > tmp/ca-clone.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_database=ca
pki_ds_password=Secret.123

#pki_ds_secure_connection=True
#pki_ds_ldaps_port=636
#pki_ds_secure_connection_ca_nickname=$MASTER
#pki_ds_secure_connection_ca_pem_file=$MASTER.crt

pki_security_domain_hostname=$MASTER
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_clone=True
pki_clone_replicate_schema=True
pki_clone_uri=https://$MASTER:8443

# Dogtag 10.3
#pki_server_pkcs12_path=$PWD/tmp/ca-certs.p12
#pki_server_pkcs12_password=Secret.123

# Dogtag 10.2
pki_clone_pkcs12_password=Secret.123
pki_clone_pkcs12_path=$PWD/tmp/ca-certs.p12

# PKI 10
pki_ca_signing_nickname=ca_signing
pki_ocsp_signing_nickname=ca_ocsp_signing
pki_audit_signing_nickname=ca_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem

# PKI 9
#pki_ca_signing_nickname=caSigningCert cert-pki-ca
#pki_ocsp_signing_nickname=ocspSigningCert cert-pki-ca
#pki_audit_signing_nickname=auditSigningCert cert-pki-ca
#pki_sslserver_nickname=Server-Cert cert-pki-ca
#pki_subsystem_nickname=subsystemCert cert-pki-ca
EOF

pkispawn -f tmp/ca-clone.cfg -s CA
