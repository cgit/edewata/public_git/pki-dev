#!/bin/sh -x

INSTANCE_NAME=pki-tomcat
FILE=/etc/sysconfig/pki-tomcat

# semanage port -a -t http_port_t -p tcp 8000
#sed 's/^#\(JAVA_OPTS="-Xdebug.*\)$/\1/' < $FILE > $FILE.tmp

sed 's/^\(JAVA_OPTS=".*\)"$/\1 -Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n -Djava.awt.headless=true -Xmx128M"/' < $FILE > $FILE.tmp

mv $FILE.tmp $FILE
