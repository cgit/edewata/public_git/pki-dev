#!/bin/sh -x

INSTANCE=pki-tomcat
NSSDB_DIR=/etc/dirsrv/slapd-$INSTANCE

certutil -L -d $NSSDB_DIR -n "DS Certificate"
