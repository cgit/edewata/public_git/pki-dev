#!/bin/sh -x

# enable insecure connection
ldapmodify -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: cn=config
changetype: modify
replace: nsslapd-allow-anonymous-access
nsslapd-allow-anonymous-access: on
-
replace: nsslapd-minssf
nsslapd-minssf: 0
-
replace: nsslapd-require-secure-binds
nsslapd-require-secure-binds: off
-
EOF

systemctl restart dirsrv@pki-tomcat.service
