#!/bin/sh

for handle in "$@"
do
    echo "Deleting object #$handle..."
    /usr/safenet/lunaclient/bin/cmu delete -handle $handle -force -password `cat lunasa.txt`
done

echo "Done."
