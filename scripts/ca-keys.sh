#!/bin/sh

NSSDB_PASSWORD=`grep internal= /var/lib/pki/pki-tomcat/conf/password.conf | awk -F = '{ print $2; }'`

echo $NSSDB_PASSWORD > /var/lib/pki/pki-tomcat/alias/password.txt

certutil -K -d /var/lib/pki/pki-tomcat/alias -f /var/lib/pki/pki-tomcat/alias/password.txt
