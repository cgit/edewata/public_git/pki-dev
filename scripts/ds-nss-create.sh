#!/bin/sh -x

INSTANCE=pki-tomcat
PASSWORD=Secret123
NSSDB_DIR=/etc/dirsrv/slapd-$INSTANCE

echo $PASSWORD > $NSSDB_DIR/password.txt
chown nobody.nobody $NSSDB_DIR/password.txt
chmod 400 $NSSDB_DIR/password.txt

echo "Internal (Software) Token:$PASSWORD" > $NSSDB_DIR/pin.txt
chown nobody.nobody $NSSDB_DIR/pin.txt
chmod 400 $NSSDB_DIR/pin.txt

certutil -W -d $NSSDB_DIR -f $NSSDB_DIR/password.txt

echo -e "y\n\ny\n" | \
 certutil -S -x \
 -d $NSSDB_DIR \
 -f $NSSDB_DIR/password.txt \
 -z noise.bin \
 -n "DS CA Signing Certificate" \
 -s "CN=DS CA Signing Certificate" \
 -t "CTu,Cu,Cu" \
 -m $RANDOM\
 -2 \
 --keyUsage certSigning \
 --nsCertType sslCA,smimeCA,objectSigningCA

certutil -L -d $NSSDB_DIR -n "DS CA Signing Certificate" -a > ca.crt

echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
 certutil -C -a \
 -d $NSSDB_DIR \
 -f $NSSDB_DIR/password.txt \
 -m $RANDOM \
 -i ds.csr \
 -o ds.crt \
 -c "DS CA Signing Certificate" \
 -1 -2

#echo -e "0\n1\n2\n3\n9\ny\n" | \
# certutil -S -x \
# -d $NSSDB_DIR \
# -f $NSSDB_DIR/password.txt \
# -z noise.bin \
# -n "$HOSTNAME" \
# -s "CN=$HOSTNAME" \
# -t "CT,C,C" \
# -m $RANDOM\
# -1

certutil -L -d $NSSDB_DIR -n "$HOSTNAME" -a > $HOSTNAME.crt
