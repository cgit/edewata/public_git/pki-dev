#!/bin/sh -x

mkdir -p tmp

pki-server subsystem-cert-export ca signing \
  --csr-file tmp/ca_signing.csr \
  --pkcs12-file tmp/ca-certs.p12 \
  --pkcs12-password-file password.txt

pki pkcs12-cert-find --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt
pki pkcs12-key-find --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt
