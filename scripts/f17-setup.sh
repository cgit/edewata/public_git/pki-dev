#!/bin/sh -x

# resteasy
yum install -y\
	apache-mime4j\
	bea-stax\
	bouncycastle\
	bouncycastle-mail\
	cglib\
	codehaus-parent\
	dnsjava\
	glassfish-jaxb\
	glassfish-jaxb-api\
	google-guice\
	httpunit\
	jackson\
	jboss-web\
	jcip-annotations\
	istack-commons\
	scannotation\
	snakeyaml\
	glassfish-fi\
	txw2\
	tomcat\
	jetty-version-maven-plugin\
	maven\
	maven-checkstyle-plugin\
	maven-compiler-plugin\
	maven-deploy-plugin\
	maven-install-plugin\
	maven-javadoc-plugin\
	maven-jaxb2-plugin\
	maven-plugin-cobertura\
	maven-pmd-plugin\
	maven-resources-plugin\
	maven-site-plugin\
	maven-source-plugin\
	maven-surefire-plugin\
	maven-surefire-report-plugin

