#!/bin/sh -x

mkdir -p tmp

#USER=`cat user.txt`
TOKEN=softcard

cat > tmp/ca-nfast.cfg << EOF
[DEFAULT]
pki_pin=Secret.123
pki_hsm_enable=True

pki_hsm_libfile=/opt/nfast/toolkits/pkcs11/libcknfast.so
pki_hsm_modulename=nfast
pki_token_name=$TOKEN
pki_token_password=Secret.123
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_database=ca
pki_ds_password=Secret.123

pki_security_domain_name=EXAMPLE

pki_ca_signing_nickname=ca_signing
#pki_ca_signing_nickname=$USER/%(pki_instance_name)s/ca_signing
#pki_ca_signing_token=internal
pki_ca_signing_token=$TOKEN

pki_ocsp_signing_nickname=ca_ocsp_signing
#pki_ocsp_signing_nickname=$USER/%(pki_instance_name)s/ca_ocsp_signing
#pki_ocsp_signing_token=internal
pki_ocsp_signing_token=$TOKEN

pki_audit_signing_nickname=ca_audit_signing
#pki_audit_signing_nickname=$USER/%(pki_instance_name)s/ca_audit_signing
#pki_audit_signing_token=internal
pki_audit_signing_token=$TOKEN

pki_sslserver_nickname=sslserver
#pki_sslserver_nickname=$USER/%(pki_instance_name)s/sslserver/%(pki_hostname)s
#pki_sslserver_token=internal
pki_sslserver_token=$TOKEN

pki_subsystem_nickname=subsystem
#pki_subsystem_nickname=$USER/%(pki_instance_name)s/subsystem
#pki_subsystem_token=internal
pki_subsystem_token=$TOKEN
EOF

pkispawn -f tmp/ca-nfast.cfg -s CA
