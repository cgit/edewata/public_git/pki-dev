#!/bin/sh -x

PROJECT_DIR=`cd ../.. ; pwd`

BUILD_DIR=$HOME/build/pki-theme
COMPOSE=$PROJECT_DIR/pki/scripts/compose_dogtag_pki_theme_packages

mkdir -p $BUILD_DIR
cd $BUILD_DIR

rm -rf rpmbuild
mkdir -p rpmbuild

$COMPOSE --work-dir $BUILD_DIR/rpmbuild rpms

rm -rf repo
mkdir -p repo
mv `find rpmbuild/RPMS -name *.rpm` repo
#createrepo repo
