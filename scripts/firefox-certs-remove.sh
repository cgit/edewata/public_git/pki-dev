#!/bin/sh

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

CA_INSTANCE_NAME=pki-tomcat
FIREFOX_DIR=$home/.mozilla/firefox
#PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`
PROFILE=$HOSTNAME

echo cd $FIREFOX_DIR/$PROFILE
cd $FIREFOX_DIR/$PROFILE

certutil -D -n "caadmin" -d .
certutil -D -n "caagent" -d .
certutil -D -n "kraadmin" -d .
certutil -D -n "kraagent" -d .
certutil -D -n "ocspadmin" -d .
certutil -D -n "tksadmin" -d .
certutil -D -n "tpsadmin" -d .
certutil -D -n "sslserver" -d .
certutil -D -n "ca_signing" -d .
