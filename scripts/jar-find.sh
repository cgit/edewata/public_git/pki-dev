#!/bin/sh

if [ $# != 2 ]; then
    echo "usage: jar-find <dir> <class>"
    exit 1
fi

dir=$1

shift
class=$1

while [ "$2" ]
do
    class="$class\|$2"
    shift
done

echo Searching for $class in $dir...

for file in `find $dir -name '*.jar'`
do
    result=`jar tvf $file | awk '{print $8;}' | grep '.class$' | sed 's/\.class$//' | sed 's/\//./g' | grep $class`
    if [ "$result" ]
    then
        echo "## $file"
        jar tvf $file | grep $class
    fi
done
