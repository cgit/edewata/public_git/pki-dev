#!/bin/sh -x

cd ~/Downloads

BASE_URL=http://kojipkgs.fedoraproject.org/packages
PACKAGE=pki-console
VERSION=10.3.5
RELEASE=1
OS=fc24

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-console-$VERSION-$RELEASE.$OS.noarch.rpm
