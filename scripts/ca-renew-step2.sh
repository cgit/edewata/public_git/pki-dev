#!/bin/sh -x

timedatectl set-ntp false
timedatectl set-time 2018-11-26

./tomcat-restart.sh

sleep 5

pki ca-cert-request-submit --profile caManualRenewal --serial 0x2
pki ca-cert-request-submit --profile caManualRenewal --serial 0x3
pki ca-cert-request-submit --profile caManualRenewal --serial 0x4
pki ca-cert-request-submit --profile caManualRenewal --serial 0x5

pki ca-cert-request-submit --profile caManualRenewal --serial 0x6

#pki -U https://$HOSTNAME:8443 \
#    -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin client-cert-request \
#    "CN=PKI Administrator,E=caadmin@example.com,OU=pki-tomcat,O=EXAMPLE" \
#    --profile caManualRenewal

pki -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin -c Secret.123 ca-cert-request-review 0x7 --action approve
pki -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin -c Secret.123 ca-cert-request-review 0x8 --action approve
pki -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin -c Secret.123 ca-cert-request-review 0x9 --action approve
pki -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin -c Secret.123 ca-cert-request-review 0xa --action approve
pki -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin -c Secret.123 ca-cert-request-review 0xb --action approve

pki ca-cert-show 0x7 --output ca_ocsp_signing.crt
pki ca-cert-show 0x8 --output sslserver.crt
pki ca-cert-show 0x9 --output subsystem.crt
pki ca-cert-show 0xa --output ca_audit_signing.crt

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin ca-user-cert-add caadmin --serial 0xb

#pki ca-cert-show 0xb --output caadmin.crt
certutil -D -d ~/.dogtag/pki-tomcat/ca/alias -n caadmin
pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 client-cert-import caadmin --serial 0xb

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin ca-user-cert-del caadmin "2;6;CN=CA Signing Certificate,OU=pki-tomcat,O=EXAMPLE;CN=PKI Administrator,E=caadmin@example.com,OU=pki-tomcat,O=EXAMPLE"

./tomcat-stop.sh

pki-server subsystem-cert-update ca ocsp_signing --cert ca_ocsp_signing.crt --replace
pki-server subsystem-cert-update ca sslserver --cert sslserver.crt --replace
pki-server subsystem-cert-update ca subsystem --cert subsystem.crt --replace
pki-server subsystem-cert-update ca audit_signing --cert ca_audit_signing.crt --replace

#pki-server subsystem-cert-update ca audit_signing --cert ca_audit_signing.crt
#pki-server subsystem-cert-update ca sslserver --cert sslserver.crt
#pki-server subsystem-cert-update ca subsystem --cert subsystem.crt
#pki-server subsystem-cert-update ca audit_signing --cert ca_audit_signing.crt

#certutil -D -d /var/lib/pki/pki-tomcat/alias -n ca_ocsp_signing
#certutil -D -d /var/lib/pki/pki-tomcat/alias -n sslserver
#certutil -D -d /var/lib/pki/pki-tomcat/alias -n subsystem
#certutil -D -d /var/lib/pki/pki-tomcat/alias -n ca_audit_signing

#certutil -A -d /var/lib/pki/pki-tomcat/alias -n ca_ocsp_signing -i ca_ocsp_signing.crt -t "u,u,u"
#certutil -A -d /var/lib/pki/pki-tomcat/alias -n sslserver -i sslserver.crt -t "u,u,u"
#certutil -A -d /var/lib/pki/pki-tomcat/alias -n subsystem -i subsystem.crt -t "u,u,u"
#certutil -A -d /var/lib/pki/pki-tomcat/alias -n ca_audit_signing -i ca_audit_signing.crt -t "u,u,Pu"

./tomcat-start.sh

pki-server subsystem-cert-find ca
certutil -L -d /var/lib/pki/pki-tomcat/alias
