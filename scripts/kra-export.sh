#!/bin/sh -x

grep "internal=" /var/lib/pki/pki-tomcat/conf/password.conf | awk -F= '{print $2}' > internal.txt

PKCS12Export -debug -d /var/lib/pki/pki-tomcat/alias -p internal.txt -w password.txt -o kra_backup_keys.p12
