#!/bin/sh -x

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-user-add causer --fullName "CA Admin"
pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-group-member-add "Administrators" causer

REQUEST_ID=`pki -c Secret123 client-cert-request uid=causer | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-user-cert-add causer --serial $CERT_ID
pki -c Secret123 client-cert-import causer --serial $CERT_ID

pki -c Secret123 client-cert-show causer --pkcs12 causer.p12 --pkcs12-password Secret123
