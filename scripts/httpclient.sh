#!/bin/sh

PKCS10Client -d ~/.dogtag/pki-tomcat/ca/alias -p Secret.123 \
  -a rsa -l 1024 \
  -n "uid=testuser,ou=people,dc=example,dc=com" \
  -o /tmp/httpclient.pem

AtoB /tmp/httpclient.pem /tmp/httpclient.bin

cat > httpclient.cfg << EOF
host=$HOSTNAME
port=8443
secure=true

input=/tmp/httpclient.bin
output=/tmp/httpclient.out

tokenname=internal
dbdir=/root/.dogtag/pki-tomcat/ca/alias
clientmode=false
password=Secret.123
nickname=caadmin

servlet=/ca/ee/ca/profileSubmit
EOF

HttpClient httpclient.cfg
