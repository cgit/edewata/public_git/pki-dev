#!/bin/sh

mkdir -p tmp

cat > tmp/ca_signing-ext.cfg << EOF
[ ca_extensions ]

subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always, issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, nonRepudiation, keyCertSign, cRLSign
EOF

openssl x509 -req \
 -CA tmp/root.crt \
 -CAkey tmp/root.key \
 -CAcreateserial \
 -in tmp/ca_signing.csr \
 -out tmp/ca_signing.crt \
 -extfile tmp/ca_signing-ext.cfg \
 -extensions ca_extensions \
 -set_serial 1

openssl x509 -text -noout -in tmp/ca_signing.crt
