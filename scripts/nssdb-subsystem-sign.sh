#!/bin/sh

AKID="`cat nssdb/ca_signing.skid`"
echo "AKID: ${AKID}"

OCSP="`cat nssdb/ocsp_url`"
echo "OCSP: ${OCSP}"

echo -e "y\n${AKID}\n\n\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/subsystem.csr \
 -o nssdb/subsystem.crt \
 -c "ca_signing" \
 -3 \
 --extAIA \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage clientAuth,serverAuth

certutil -A -d nssdb -n "subsystem" -i nssdb/subsystem.crt -t ",,"

openssl x509 -text -noout -in nssdb/subsystem.crt
