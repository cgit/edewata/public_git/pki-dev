#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=$HOSTNAME,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/sslserver.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage serverAuth

openssl req -inform der -in nssdb/sslserver.csr.der -out nssdb/sslserver.csr
