#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=PKI Adminstrator,E=admin@example.com,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/admin.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage clientAuth,emailProtection

openssl req -inform der -in nssdb/admin.csr.der -out nssdb/admin.csr
