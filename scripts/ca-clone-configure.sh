#!/bin/sh -x

PKI_DEV_SRC=`cd .. ; pwd`

INSTANCE_NAME=pki-caclone
PASSWORD=Secret123
PIN=`grep preop.pin= /var/lib/$INSTANCE_NAME/conf/CS.cfg | awk -F= '{ print $2; }'`

REALM=EXAMPLE-COM
CERTS=$PKI_DEV_SRC/certs/caclone
rm -rf $CERTS
mkdir -p $CERTS

./ca-clone-certs.sh

pkisilent ConfigureCA \
        -cs_hostname "$HOSTNAME" \
        -cs_port "9444" \
        -preop_pin "$PIN" \
        -client_certdb_dir "$CERTS" \
        -client_certdb_pwd "$PASSWORD" \
        -token_name "internal" \
        -domain_name "$REALM" \
        -subsystem_name "Certificate Authority Clone" \
	-clone "true" \
	-clone_uri "https://$HOSTNAME:9443" \
	-clone_p12_file "ca-server-certs.p12" \
	-clone_p12_password "$PASSWORD" \
        -sd_hostname "$HOSTNAME" \
        -sd_admin_port 9443 \
        -sd_ssl_port 9443 \
        -sd_agent_port 9443 \
        -sd_admin_name "caadmin" \
        -sd_admin_password "$PASSWORD" \
        -ldap_host "localhost" \
        -ldap_port "390" \
        -base_dn "dc=ca,dc=example,dc=com" \
        -db_name "example.com-$INSTANCE_NAME" \
        -bind_dn "cn=Directory Manager" \
        -bind_password "$PASSWORD" \
        -remove_data "true" \
        -key_type rsa \
        -key_size 2048 \
        -key_algorithm SHA256withRSA \
        -signing_signingalgorithm SHA256withRSA \
        -save_p12 true \
        -backup_fname "$CERTS/caclone-server-certs.p12" \
        -backup_pwd "$PASSWORD" \
        -ca_sign_cert_subject_name "CN=Certificate Authority,O=$REALM" \
        -ca_ocsp_cert_subject_name "CN=OCSP Signing Certificate,O=$REALM" \
        -ca_server_cert_subject_name "CN=$HOSTNAME,O=$REALM" \
        -ca_subsystem_cert_subject_name "CN=CA Subsystem Certificate,O=$REALM" \
        -ca_audit_signing_cert_subject_name "CN=CA Audit Signing Certificate,O=$REALM" \
        -admin_user "caadmin" \
        -agent_name "caadmin" \
        -admin_email "caadmin@example.com" \
        -admin_password "$PASSWORD" \
        -agent_key_size 2048 \
        -agent_key_type rsa \
        -agent_cert_subject "CN=caadmin,UID=caadmin,E=caadmin@example.com,O=$REALM"


echo $PASSWORD > "$CERTS/password.txt"
PKCS12Export -d "$CERTS" -o "$CERTS/caclone-client-certs.p12" -p "$CERTS/password.txt" -w "$CERTS/password.txt"

systemctl restart pki-cad@$INSTANCE_NAME.service
