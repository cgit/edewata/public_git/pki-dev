#!/bin/sh -x

INSTANCE_NAME=tps-master
FILE=/etc/sysconfig/$INSTANCE_NAME

# semanage port -a -t http_port_t -p tcp 8000
sed 's/^#\(JAVA_OPTS="-Xdebug.*\)$/\1/' < $FILE > $FILE.tmp
mv $FILE.tmp $FILE
