#!/bin/sh -x

rm -rf /tmp/ca.p12
rm -rf /tmp/ca_signing.csr

/bin/cp ca.p12 /tmp
/bin/cp ca_signing.csr /tmp

pkispawn -vv -f ca-restore.cfg -s CA --stop-at configuration
#systemctl start pki-tomcatd@pki-tomcat.service
#sleep 5
pkispawn -vv -f ca-restore.cfg -s CA --start-from finalization

/bin/cp /root/.dogtag/pki-tomcat/ca_admin.cert .
/bin/cp /root/.dogtag/pki-tomcat/ca_admin_cert.p12 .
/bin/cp /root/.dogtag/pki-tomcat/ca/pkcs12_password.conf ca_admin_cert.txt
/bin/cp /var/lib/pki/pki-tomcat/alias/ca_backup_keys.p12 .
echo $HOSTNAME > master.txt
