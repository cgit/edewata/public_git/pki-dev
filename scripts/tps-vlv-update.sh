#!/bin/sh -x

ldapdelete -x -D "cn=Directory Manager" -w Secret123 << EOF
cn=listTokensIndex,cn=listTokens,cn=tps,cn=ldbm database,cn=plugins,cn=config
cn=listTokens,cn=tps,cn=ldbm database,cn=plugins,cn=config
cn=listActivitiesIndex,cn=listActivities,cn=tps,cn=ldbm database,cn=plugins,cn=config
cn=listActivities,cn=tps,cn=ldbm database,cn=plugins,cn=config
EOF

/bin/cp /usr/share/pki/tps/conf/vlv.ldif .
sed -i "s/{instanceId}/pki-tomcat/g" vlv.ldif
sed -i "s/{database}/tps/g" vlv.ldif
sed -i "s/{rootSuffix}/dc=tps,dc=example,dc=com/" vlv.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f vlv.ldif

systemctl stop dirsrv@pki-tomcat.service
vlvindex -Z pki-tomcat -n tps -T listTokensIndex
vlvindex -Z pki-tomcat -n tps -T listActivitiesIndex
systemctl start dirsrv@pki-tomcat.service

