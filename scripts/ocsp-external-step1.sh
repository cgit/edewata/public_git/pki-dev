#!/bin/sh -x

mkdir -p tmp

CA_HOSTNAME=`cat tmp/ca.hostname`

cat > tmp/ocsp-external-step1.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[OCSP]
pki_admin_email=ocspadmin@example.com
pki_admin_name=ocspadmin
pki_admin_nickname=ocspadmin
pki_admin_password=Secret.123
pki_admin_uid=ocspadmin

#pki_backup_keys=True
#pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ocsp,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ocsp

pki_security_domain_hostname=$CA_HOSTNAME
pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_token_password=Secret.123

pki_external=True
pki_external_step_two=False

pki_ca_signing_nickname=ca_signing
pki_ocsp_signing_nickname=ocsp_signing
pki_audit_signing_nickname=ocsp_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem

#pki_external_signing_csr_path=tmp/ocsp_signing.csr
#pki_external_subsystem_csr_path=tmp/subsystem.csr
#pki_external_sslserver_csr_path=tmp/sslserver.csr
#pki_external_admin_csr_path=tmp/ocsp_admin.csr
#pki_external_audit_signing_csr_path=tmp/ocsp_audit_signing.csr

pki_ocsp_signing_csr_path=tmp/ocsp_signing.csr
pki_subsystem_csr_path=tmp/subsystem.csr
pki_sslserver_csr_path=tmp/sslserver.csr
pki_audit_signing_csr_path=tmp/ocsp_audit_signing.csr
pki_admin_csr_path=tmp/ocsp_admin.csr
EOF

pkispawn -f tmp/ocsp-external-step1.cfg -s OCSP
