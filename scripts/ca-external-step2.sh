#!/bin/sh -x

mkdir -p tmp

cat > tmp/ca-external-step2.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_backup_keys=True
pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_database=ca
pki_ds_password=Secret.123

pki_security_domain_name=EXAMPLE
pki_token_password=Secret.123

pki_external=True
pki_external_step_two=True

#pki_external_csr_path=tmp/ca_signing.csr
pki_ca_signing_csr_path=tmp/ca_signing.csr

#pki_external_ca_cert_path=tmp/ca_signing.crt
pki_ca_signing_cert_path=tmp/ca_signing.crt

#pki_external_ca_cert_chain_nickname=external
#pki_external_ca_cert_chain_nickname=Root CA Signing Certificate - ROOT
#pki_cert_chain_nickname=Root CA Signing Certificate - ROOT

#pki_external_ca_cert_chain_path=tmp/cert_chain.p7b
#pki_external_ca_cert_chain_path=tmp/external.crt
#pki_cert_chain_path=tmp/external.crt

pki_ca_signing_nickname=ca_signing
pki_ocsp_signing_nickname=ca_ocsp_signing
pki_subsystem_nickname=subsystem
pki_sslserver_nickname=sslserver
pki_audit_signing_nickname=ca_audit_signing
EOF

pkispawn -f tmp/ca-external-step2.cfg -s CA

echo $HOSTNAME > tmp/ca.hostname
