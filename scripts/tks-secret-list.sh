#!/bin/sh

INSTANCE_DIR=/var/lib/pki/tks-master
grep "internal=" $INSTANCE_DIR/conf/password.conf | sed "s/internal=//" > $INSTANCE_DIR/conf/internal.txt

echo certutil -K -d $INSTANCE_DIR/alias -f $INSTANCE_DIR/conf/internal.txt
certutil -K -d $INSTANCE_DIR/alias -f $INSTANCE_DIR/conf/internal.txt

echo tkstool -L -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt -h all
tkstool -L -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt -h all

