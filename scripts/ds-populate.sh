#!/bin/sh

/bin/cp /usr/share/pki/ca/conf/index.ldif .
sed -i "s/{database}/ca/" index.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f index.ldif

ldapadd -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: dc=ca,dc=example,dc=com
objectClass: top
objectClass: domain
dc: ca
EOF

/bin/cp /usr/share/pki/ca/conf/db.ldif .
sed -i "s/{rootSuffix}/dc=ca,dc=example,dc=com/" db.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f db.ldif

/bin/cp /usr/share/pki/ca/conf/acl.ldif .
sed -i "s/{rootSuffix}/dc=ca,dc=example,dc=com/" acl.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f acl.ldif

/bin/cp /usr/share/pki/server/conf/manager.ldif .
sed -i "s/{rootSuffix}/dc=ca,dc=example,dc=com/" manager.ldif
sed -i "s/{dbuser}/uid=pkidbuser,ou=people,dc=ca,dc=example,dc=com/" manager.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f manager.ldif

/bin/cp /usr/share/pki/ca/conf/vlv.ldif .
sed -i "s/{instanceId}/pki-tomcat/g" vlv.ldif
sed -i "s/{database}/ca/g" vlv.ldif
sed -i "s/{rootSuffix}/dc=ca,dc=example,dc=com/" vlv.ldif
ldapadd -x -D "cn=Directory Manager" -w Secret123 -f vlv.ldif
