#!/bin/sh

OUTPUT=nssdb/ca_signing.csr

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -h internal \
 -f nssdb/password.txt \
 -s "CN=CA Signing Certificate,O=EXAMPLE" \
 -z nssdb/noise.bin \
 -k rsa \
 -g 2048 \
 -Z SHA512 \
 -2 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 -o nssdb/ca.csr.der

BtoA nssdb/ca.csr.der nssdb/ca.csr.pem
echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > $OUTPUT
cat nssdb/ca.csr.pem >> $OUTPUT
echo "-----END NEW CERTIFICATE REQUEST-----" >> $OUTPUT

rm nssdb/ca.csr.der
rm nssdb/ca.csr.pem
