#!/bin/sh -x

WORK_DIR=`pwd`
PROJECT_DIR=`cd ../.. ; pwd`
COMPONENT=core
ARCH=`uname -i`

mkdir -p $WORK_DIR/build
rm -rf $WORK_DIR/build/$COMPONENT

cp fedora-17-${ARCH}.cfg /etc/mock/mock.cfg

cd $PROJECT_DIR
rm -rf packages
mkdir -p packages

pki/scripts/compose_pki_${COMPONENT}_packages hybrid_srpm

mv packages $WORK_DIR/build/$COMPONENT
cd $WORK_DIR/build/$COMPONENT

mock --init -r mock
mock -v -r mock --installdeps `find SRPMS -name *.src.rpm`
mock -v -r mock --no-cleanup-after --no-clean `find SRPMS -name *.src.rpm`

mv /var/lib/mock/fedora-17-${ARCH}/result/*.src.rpm SRPMS

mkdir -p repo
mv /var/lib/mock/fedora-17-${ARCH}/result/*.rpm repo
#createrepo repo

mv /var/lib/mock/fedora-17-${ARCH}/result/* .
rm -rf /var/lib/mock/*
