#!/bin/sh -x

mkdir -p tmp

ISSUING_CA=`cat tmp/root.txt`

cat > tmp/ca-sub.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_subordinate=True
pki_issuing_ca_hostname=$ISSUING_CA
pki_issuing_ca_https_port=8443
pki_ca_signing_subject_dn=cn=CA Signing Certificate,o=EXAMPLE

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_database=ca
pki_ds_password=Secret.123

pki_security_domain_hostname=$ISSUING_CA
pki_security_domain_https_port=8443
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_ca_signing_nickname=ca_signing
pki_ocsp_signing_nickname=ca_ocsp_signing
pki_audit_signing_nickname=ca_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -v -f tmp/ca-sub.cfg -s CA
