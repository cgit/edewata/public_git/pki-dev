#!/bin/sh -x

mkdir -p tmp

echo $HOSTNAME > tmp/master.txt

grep "internal=" /var/lib/pki/pki-tomcat/conf/password.conf | awk -F= '{print $2}' > tmp/internal.txt

#PKCS12Export -debug -d /var/lib/pki/pki-tomcat/alias -p tmp/internal.txt -w password.txt -o tmp/ca_backup_keys.p12
#pki pkcs12-cert-find --pkcs12-file tmp/ca_backup_keys.p12 --pkcs12-password-file password.txt

pki-server ca-clone-prepare --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt
pki pkcs12-cert-find --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt

#cp ~/.dogtag/pki-tomcat/ca_admin.cert tmp
cp ~/.dogtag/pki-tomcat/ca_admin_cert.p12 tmp
