#!/bin/sh

REQUEST_ID=$1

SRC_DIR=`cd ../.. ; pwd`
INSTANCE_NAME=ca-master
CLIENT_CERT_DIR=$HOME/.dogtag/${INSTANCE_NAME}/ca/alias

# Approve request as an agent
pki -d $CLIENT_CERT_DIR -c Secret123 -n caadmin cert-request-review "$REQUEST_ID" --action approve
