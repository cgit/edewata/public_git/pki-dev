#!/bin/sh -x

grep "internal=" /var/lib/pki/pki-tomcat/conf/password.conf | awk -F= '{print $2}' > tmp/internal.txt
#PKCS12Export -debug -d /var/lib/pki/pki-tomcat/alias -p tmp/internal.txt -w password.txt -o tmp/ca-certs.p12
PKCS12Export -d /var/lib/pki/pki-tomcat/alias -p tmp/internal.txt -w password.txt -o tmp/ca-certs.p12

pki pkcs12-cert-find --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt
pki pkcs12-key-find --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > tmp/ca_signing.csr
sed -n "/^ca.signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> tmp/ca_signing.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> tmp/ca_signing.csr

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > tmp/ca_ocsp_signing.csr
sed -n "/^ca.ocsp_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> tmp/ca_ocsp_signing.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> tmp/ca_ocsp_signing.csr

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > tmp/sslserver.csr
sed -n "/^ca.sslserver.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> tmp/sslserver.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> tmp/sslserver.csr

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > tmp/subsystem.csr
sed -n "/^ca.subsystem.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> tmp/subsystem.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> tmp/subsystem.csr

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > tmp/ca_audit_signing.csr
sed -n "/^ca.audit_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> tmp/ca_audit_signing.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> tmp/ca_audit_signing.csr

#pki-server ca-clone-prepare --pkcs12-file tmp/ca-certs.p12 --pkcs12-password-file password.txt

cp ~/.dogtag/pki-tomcat/ca_admin.cert tmp
cp ~/.dogtag/pki-tomcat/ca_admin_cert.p12 tmp
