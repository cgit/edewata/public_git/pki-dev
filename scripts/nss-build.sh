#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

cd $SRC_DIR
gendiff nss .original > ~/rpmbuild/SOURCES/nss-debug.patch

rpmbuild -bb ~/rpmbuild/SPECS/nss.spec
