#!/bin/sh -x

mkdir -p tmp

pk12util \
 -d /etc/pki/pki-tomcat/alias \
 -K Secret.123 \
 -o tmp/sslserver.p12 \
 -W Secret.123 \
 -n sslserver

openssl pkcs12 \
 -in tmp/sslserver.p12 \
 -passin pass:Secret.123 \
 -out tmp/sslserver.pem \
 -nodes

openssl pkcs12 \
 -in tmp/sslserver.p12 \
 -passin pass:Secret.123 \
 -out tmp/sslserver.key \
 -nodes \
 -nocerts

openssl pkcs12 \
 -in tmp/sslserver.p12 \
 -passin pass:Secret.123 \
 -out tmp/sslserver.crt \
 -clcerts \
 -nokeys

openssl pkcs12 \
 -in tmp/sslserver.p12 \
 -passin pass:Secret.123 \
 -out tmp/sslserver.p7b \
 -nokeys

openssl pkcs12 \
 -in tmp/sslserver.p12 \
 -passin pass:Secret.123 \
 -out tmp/sslserver.chain \
 -cacerts \
 -nokeys

pki -c Secret.123 client-init --force
#python ca-python-test.py
