#!/bin/sh -x

. ./ra-include.sh

PIN=`grep preop.pin= $INSTANCE_ROOT/$RA_INSTANCE_NAME/conf/CS.cfg | awk -F= '{ print $2; }'`

CERTS=$SRC_DIR/pki-dev/certs/ra
rm -rf $CERTS
mkdir -p $CERTS

if [ "$RA_SECURE_PORT" == "" ]; then
    PORT="$RA_ADMIN_SECURE_PORT"
else
    PORT="$RA_SECURE_PORT"
fi

pkisilent ConfigureRA \
        -cs_hostname $HOSTNAME \
        -cs_port $PORT \
        -cs_clientauth_port $PORT \
        -preop_pin $PIN \
        -client_certdb_dir "$CERTS" \
        -client_certdb_pwd "$PASSWORD" \
        -token_name "internal" \
        -sd_hostname "$HOSTNAME" \
        -sd_admin_port 8443 \
        -sd_ssl_port 8443 \
        -sd_agent_port 8443 \
        -sd_admin_name "caadmin" \
        -sd_admin_password "$PASSWORD" \
        -domain_name "$REALM" \
        -subsystem_name "$RA_SUBSYSTEM_NAME" \
        -key_type rsa \
        -key_size 2048 \
        -ra_server_cert_subject_name "$RA_SERVER_CERT_SUBJECT_NAME" \
        -ra_subsystem_cert_subject_name "$RA_SUBSYSTEM_CERT_SUBJECT_NAME" \
        -ca_hostname "$HOSTNAME" \
        -ca_port 8080 \
        -ca_ssl_port 8443 \
        -ca_admin_port 8443 \
        -admin_user "$RA_ADMIN_USER" \
        -agent_name "$RA_ADMIN_NAME" \
        -admin_email "$RA_ADMIN_EMAIL" \
        -admin_password "$RA_ADMIN_PASSWORD" \
        -agent_key_size 2048 \
        -agent_key_type rsa \
        -agent_cert_subject "$RA_ADMIN_CERT_SUBJECT"

echo $PASSWORD > "$CERTS/password.txt"
PKCS12Export -d "$CERTS" -o "$CERTS/ra-client-certs.p12" -p "$CERTS/password.txt" -w "$CERTS/password.txt"

systemctl restart pki-rad@$RA_INSTANCE_NAME.service
