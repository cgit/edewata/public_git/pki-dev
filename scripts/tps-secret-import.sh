#!/bin/sh

INSTANCE_DIR=/var/lib/pki-tps
grep "internal:" $INSTANCE_DIR/conf/password.conf | sed "s/internal://" > $INSTANCE_DIR/conf/internal.txt
tkstool -I -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt
