#!/bin/sh

# generate self-signed CA certificate

echo -e "y\n\ny\n" | \
 certutil -S \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -n "CA Signing Certificate" \
 -s "CN=CA Signing Certificate,O=EXAMPLE" \
 -x \
 -t "CTu,Cu,Cu" \
 -m $RANDOM\
 -2 \
 --keyUsage certSigning \
 --nsCertType sslCA,smimeCA,objectSigningCA

certutil -L -d nssdb -n "CA Signing Certificate" -a > nssdb/ca.crt
