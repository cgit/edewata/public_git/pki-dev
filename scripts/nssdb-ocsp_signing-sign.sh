#!/bin/sh

AKID="`cat nssdb/ca_signing.skid`"
echo "AKID: ${AKID}"

OCSP="`cat nssdb/ocsp_url`"
echo "OCSP: ${OCSP}"

echo -e "y\n${AKID}\n\n\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/ocsp_signing.csr \
 -o nssdb/ocsp_signing.crt \
 -c "ca_signing" \
 -3 \
 --extAIA \
 --extKeyUsage ocspResponder \
 --extGeneric 1.3.6.1.5.5.7.48.1.5:not-critical:/dev/null

certutil -A -d nssdb -n "ocsp_signing" -i nssdb/ocsp_signing.crt -t ",,"

openssl x509 -text -noout -in nssdb/ocsp_signing.crt
