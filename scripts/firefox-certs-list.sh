#!/bin/sh -x

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

FIREFOX_DIR=$home/.mozilla/firefox
#PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`
PROFILE=$HOSTNAME

certutil -L -d $FIREFOX_DIR/$PROFILE
