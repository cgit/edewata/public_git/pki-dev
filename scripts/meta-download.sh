#!/bin/sh -x

cd ~/Downloads

BASE_URL=http://kojipkgs.fedoraproject.org/packages
PACKAGE=dogtag-pki
VERSION=10.3.5
RELEASE=1
OS=fc24

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/dogtag-pki-$VERSION-$RELEASE.$OS.noarch.rpm
