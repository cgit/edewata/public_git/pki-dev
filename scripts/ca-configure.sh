#!/bin/sh -x

. ./ca-include.sh

PIN=`grep preop.pin= $INSTANCE_ROOT/$CA_INSTANCE_NAME/conf/CS.cfg | awk -F= '{ print $2; }'`

CERTS=$SRC_DIR/pki-dev/certs/ca
rm -rf $CERTS
mkdir -p $CERTS

if [ "$CA_SECURE_PORT" == "" ]; then
    PORT="$CA_ADMIN_SECURE_PORT"
else
    PORT="$CA_SECURE_PORT"
fi

pkisilent ConfigureCA \
        -cs_hostname $HOSTNAME \
        -cs_port $PORT \
        -preop_pin $PIN \
        -client_certdb_dir "$CERTS" \
        -client_certdb_pwd "$PASSWORD" \
        -token_name "internal" \
        -domain_name "$REALM" \
        -subsystem_name "$CA_SUBSYSTEM_NAME" \
        -ldap_host "$CA_LDAP_HOST" \
        -ldap_port "$CA_LDAP_PORT" \
        -base_dn "$CA_LDAP_BASE_DN" \
        -db_name "$CA_LDAP_DATABASE" \
        -bind_dn "$CA_LDAP_BIND_DN" \
        -bind_password "$CA_LDAP_PASSWORD" \
        -remove_data true \
        -key_type rsa \
        -key_size 2048 \
        -key_algorithm SHA256withRSA \
        -signing_signingalgorithm SHA256withRSA \
        -save_p12 true \
        -backup_fname "$CERTS/ca-server-certs.p12" \
        -backup_pwd "$PASSWORD" \
        -ca_sign_cert_subject_name "$CA_SIGN_CERT_SUBJECT_NAME" \
        -ca_ocsp_cert_subject_name "$CA_OCSP_CERT_SUBJECT_NAME" \
        -ca_server_cert_subject_name "$CA_SERVER_CERT_SUBJECT_NAME" \
        -ca_subsystem_cert_subject_name "$CA_SUBSYSTEM_CERT_SUBJECT_NAME" \
        -ca_audit_signing_cert_subject_name "$CA_AUDIT_SIGNING_CERT_SUBJECT_NAME" \
        -admin_user "$CA_ADMIN_USER" \
        -agent_name "$CA_ADMIN_NAME" \
        -admin_email "$CA_ADMIN_EMAIL" \
        -admin_password "$CA_ADMIN_PASSWORD" \
        -agent_key_size 2048 \
        -agent_key_type rsa \
        -agent_cert_subject "$CA_ADMIN_CERT_SUBJECT"

echo $PASSWORD > "$CERTS/password.txt"
PKCS12Export -d "$CERTS" -o "$CERTS/ca-client-certs.p12" -p "$CERTS/password.txt" -w "$CERTS/password.txt"

systemctl restart pki-cad@$CA_INSTANCE_NAME.service
