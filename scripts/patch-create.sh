#!/bin/sh

NUMBER=$1

while [ `expr length $NUMBER` -lt 4 ]; do
    NUMBER=0$NUMBER
done

PROJECT_DIR=`cd ../.. ; pwd`
SOURCE_DIR=$PROJECT_DIR/pki
TARGET_DIR=$PROJECT_DIR/shared

cd $SOURCE_DIR
git format-patch -M -C --patience --full-index -1
SOURCE=`ls 0001-*.patch`

NAME=`echo $SOURCE | sed 's/0001-\(.*\)\.patch/\1/'`
TARGET=pki-$USER-$NUMBER-$NAME.patch

echo $TARGET
mv $SOURCE_DIR/$SOURCE $TARGET_DIR/$TARGET
