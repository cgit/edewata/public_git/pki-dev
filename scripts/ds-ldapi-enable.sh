#!/bin/sh -x

ldapmodify -h `hostname` -p 389 -D "cn=Directory Manager" -w Secret123 -x <<EOF
dn: cn=config
changetype: modify
replace: nsslapd-ldapilisten
nsslapd-ldapilisten: on
EOF

systemctl restart dirsrv@pki-tomcat.service
