#!/bin/sh -x

# enable SSL
ldapmodify -x -D "cn=Directory Manager" -w Secret123 <<EOF
dn: cn=config
changetype: modify
replace: nsslapd-security
nsslapd-security: on

dn: cn=RSA,cn=encryption,cn=config
changetype: add
objectclass: top
objectclass: nsEncryptionModule
cn: RSA
nsSSLPersonalitySSL: $HOSTNAME
nsSSLToken: internal (software)
nsSSLActivation: on
EOF

#dn: cn=encryption,cn=config
#changetype: modify
#add: nsSSL3Ciphers
#nsSSL3Ciphers: +all

systemctl restart dirsrv@pki-tomcat.service
