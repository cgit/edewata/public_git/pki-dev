#!/bin/sh

NICKNAME=$1
FILENAME=$2

INSTANCE=pki-tomcat
NSSDB_DIR=/etc/dirsrv/slapd-$INSTANCE

certutil -A -d $NSSDB_DIR -n "$NICKNAME" -i $FILENAME -a -t "CT,C,C"
