#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`
TET_DIR=$SRC_DIR/tet

cd $TET_DIR
testframework/utils/tet_shared/launch.sh $TET_DIR 2>&1 | tee tet.log
