#!/bin/sh -x

pkispawn -vv -f ca-lunasa.cfg -s CA

/bin/cp /root/.dogtag/pki-tomcat/ca_admin.cert .
/bin/cp /root/.dogtag/pki-tomcat/ca_admin_cert.p12 .
/bin/cp /root/.dogtag/pki-tomcat/ca/pkcs12_password.conf ca_admin_cert.txt
echo $HOSTNAME > master.txt
