#!/bin/sh

file=$1
old=$2
new=$3

echo "#### Comparing $file..."
jar tvf $old/$file | awk '{print $8;}' | grep -v "/$" | sort > /tmp/$file.old
jar tvf $new/$file | awk '{print $8;}' | grep -v "/$" | sort > /tmp/$file.new

# cat /tmp/$file.old
# echo "====="
# cat /tmp/$file.new
# echo "====="

diff /tmp/$file.old /tmp/$file.new
