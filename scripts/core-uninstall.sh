#!/bin/sh -x

yum erase -y\
	pki-symkey\
	pki-base\
	pki-tools\
	pki-util\
	pki-silent\
	pki-selinux\
	pki-server\
	pki-ca\
	pki-kra\
	pki-ocsp\
	pki-tks\
	pki-tps-tomcat\
	pki-javadoc\
	pki-core-debuginfo

yum erase -y\
	pki-base-java\
	python3-pki
