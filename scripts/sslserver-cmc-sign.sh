#!/bin/sh

mkdir -p tmp

cat > tmp/sslserver-cmc-request.cfg << EOF
# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default is internal).
tokenname=internal

# Nickname for agent certificate.
nickname=caadmin

# Request format: pkcs10 or crmf.
format=pkcs10

# Total number of PKCS10/CRMF requests.
numRequests=1

# Path to the PKCS10/CRMF request.
# The content must be in Base-64 encoded format.
# Multiple files are supported. They must be separated by space.
input=$PWD/tmp/sslserver.csr

# Path for the CMC request in binary format
output=$PWD/tmp/sslserver-cmc-request.bin
EOF

CMCRequest tmp/sslserver-cmc-request.cfg

cat > tmp/sslserver-cmc-submit.cfg << EOF
# PKI server host name.
host=$HOSTNAME

# PKI server port number.
port=8443

# Use secure connection.
# For secure connection with ECC, set environment variable 'export NSS_USE_DECODED_CKA_EC_POINT=1'.
secure=true

# Use client authentication.
clientmode=true

# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default: internal).
tokenname=internal

# Nickname of agent certificate.
nickname=caadmin

# CMC servlet path
#servlet=/ca/ee/ca/profileSubmitCMCFullServerCert
servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caCMCserverCert

# Path for the CMC request.
input=tmp/sslserver-cmc-request.bin

# Path for the CMC response.
output=tmp/sslserver-cmc-response.bin
EOF

HttpClient tmp/sslserver-cmc-submit.cfg

CMCResponse -d ~/.dogtag/nssdb -i tmp/sslserver-cmc-response.bin -o tmp/sslserver.crt

#BtoA tmp/sslserver-cmc-response.bin tmp/sslserver-cmc-response.b64
#echo "-----BEGIN PKCS7-----" > tmp/sslserver.crt
#cat tmp/sslserver-cmc-response.b64 >> tmp/sslserver.crt
#echo "-----END PKCS7-----" >> tmp/sslserver.crt
