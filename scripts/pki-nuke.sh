#!/bin/sh

INSTANCE_NAME=$1

if [ "$INSTANCE_NAME" == "" ]; then
    echo "usage: pki-nuke.sh <instance name>"
    exit 1
fi

echo "Deleting instance $INSTANCE_NAME"

pids="`ps -ef | grep catalina.base=/var/lib/pki/$INSTANCE_NAME | grep -v grep | awk '{print $2}'`"
for pid in $pids; do
    kill -9 $pid
done

rm -rf /etc/pki/$INSTANCE_NAME
rm -rf /etc/sysconfig/$INSTANCE_NAME
rm -rf /etc/sysconfig/pki/tomcat/$INSTANCE_NAME
rm -rf /var/lib/pki/$INSTANCE_NAME
rm -rf /var/log/pki/$INSTANCE_NAME
rm -rf /var/log/pki/pki-*.log
rm -rf $HOME/.dogtag/$INSTANCE_NAME

if [ "$INSTANCE_NAME" != "pki-tomcat" ]; then
    semanage fcontext -d "/etc/pki/$INSTANCE_NAME(/.*)?"
    semanage fcontext -d "/etc/pki/$INSTANCE_NAME/alias(/.*)?"
    semanage fcontext -d "/var/lib/pki/$INSTANCE_NAME(/.*)?"
    semanage fcontext -d "/var/log/pki/$INSTANCE_NAME(/.*)?"
fi
