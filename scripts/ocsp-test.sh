#!/bin/sh

OCSPClient \
 -d /etc/pki/pki-tomcat/alias \
 -c "ca_signing" \
 -h $HOSTNAME \
 -p 8080 \
 -t /ocsp/ee/ocsp \
 --serial 8
