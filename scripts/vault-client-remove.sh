#!/bin/python

import getopt
import subprocess
import sys

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend

import pki
import pki.client
import pki.crypto
import pki.key
import pki.kra
import pki.systemcert

def usage():
    print "usage: ipa-client-remove --user-id <user ID> --secret-id <secret ID>"

def main(argv):

    try:
        opts, _ = getopt.getopt(argv[1:], 'hv', [
            'user-id=', 'secret-id=',
            'verbose', 'help'])

    except getopt.GetoptError as e:
        print 'ERROR: ' + str(e)
        usage()
        sys.exit(1)

    verbose = False

    user_id = None
    secret_id = None

    for o, a in opts:
        if o == '--v':
            verbose = True

        elif o == '--user-id':
            user_id = a

        elif o == '--secret-id':
            secret_id = a

    subprocess.check_call(['./vault-server-remove.sh', '--user-id', user_id, '--secret-id', secret_id])

    print "Secret removed."

if __name__ == '__main__':
    main(sys.argv)
