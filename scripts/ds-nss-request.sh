#!/bin/sh -x

INSTANCE=pki-tomcat
PASSWORD=Secret123
NSSDB_DIR=/etc/dirsrv/slapd-$INSTANCE

certutil -R \
  -d $NSSDB_DIR \
  -f $NSSDB_DIR/password.txt \
  -s "CN=$HOSTNAME" \
  -g 2048 \
  -z noise.bin \
  -o ds.csr \
  -a
