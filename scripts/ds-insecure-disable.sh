#!/bin/sh -x

# disable insecure connection
LDAPTLS_CACERT=$HOSTNAME.crt \
    ldapmodify -H ldaps://$HOSTNAME:636 -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: cn=config
changetype: modify
replace: nsslapd-allow-anonymous-access
nsslapd-allow-anonymous-access: rootdse
-
replace: nsslapd-minssf
nsslapd-minssf: 56
-
replace: nsslapd-require-secure-binds
nsslapd-require-secure-binds: on
-
EOF

systemctl restart dirsrv@pki-tomcat.service
