#!/bin/sh -x

mkdir -p ~/Downloads
cd ~/Downloads

#https://kojipkgs.fedoraproject.org//packages/jss/4.2.6/40.fc24/x86_64/jss-4.2.6-40.fc24.x86_64.rpm
BASE_URL=http://kojipkgs.fedoraproject.org/packages

PACKAGE=jss
VERSION=4.2.6
RELEASE=40
#OS=fc23
OS=fc24

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/jss-$VERSION-$RELEASE.$OS.x86_64.rpm
