#!/bin/sh

pki -c Secret.123 client-init --force

pki -c Secret.123 client-cert-import "Root CA Signing Certificate" --ca-cert tmp/external.crt

pki -c Secret.123 client-cert-import "CA Signing Certificate" --ca-server

pki -c Secret.123 client-cert-import \
 --pkcs12 ~/.dogtag/pki-tomcat/ca_admin_cert.p12 \
 --pkcs12-password Secret.123

#pki -c Secret.123 pkcs12-import \
# --pkcs12-file ~/.dogtag/pki-tomcat/ca_admin_cert.p12 \
# --pkcs12-password Secret.123
