#!/bin/sh -x

. ./tps-include.sh

PIN=`grep preop.pin= $INSTANCE_ROOT/$TPS_INSTANCE_NAME/conf/CS.cfg | awk -F= '{ print $2; }'`

CERTS=$SRC_DIR/pki-dev/certs/tps
rm -rf $CERTS
mkdir -p $CERTS

PORT="$TPS_NON_CLIENTAUTH_SECURE_PORT"

pkisilent ConfigureTPS \
        -cs_hostname $HOSTNAME \
        -cs_port $PORT \
        -cs_clientauth_port $PORT \
        -preop_pin $PIN \
        -client_certdb_dir "$CERTS" \
        -client_certdb_pwd "$PASSWORD" \
        -token_name "internal" \
        -sd_hostname "$HOSTNAME" \
        -sd_admin_port 8443 \
        -sd_ssl_port 8443 \
        -sd_agent_port 8443 \
        -sd_admin_name "caadmin" \
        -sd_admin_password "$PASSWORD" \
        -domain_name "$REALM" \
        -subsystem_name "$TPS_SUBSYSTEM_NAME" \
        -ldap_host "$TPS_LDAP_HOST" \
        -ldap_port "$TPS_LDAP_PORT" \
        -base_dn "$TPS_LDAP_BASE_DN" \
        -db_name "$TPS_LDAP_DATABASE" \
        -bind_dn "$TPS_LDAP_BIND_DN" \
        -bind_password "$TPS_LDAP_PASSWORD" \
        -ldap_auth_host "$HOSTNAME" \
        -ldap_auth_port 389 \
        -ldap_auth_base_dn "dc=example,dc=com" \
        -key_type rsa \
        -key_size 2048 \
        -ss_keygen true \
        -tks_hostname "$HOSTNAME" \
        -tks_ssl_port 14443 \
        -tps_server_cert_subject_name "$TPS_SERVER_CERT_SUBJECT_NAME" \
        -tps_server_cert_nickname "Server-Cert cert-pki-tps" \
        -tps_subsystem_cert_subject_name "$TPS_SUBSYSTEM_CERT_SUBJECT_NAME" \
        -tps_subsystem_cert_nickname "subsystemCert cert-pki-tps" \
        -tps_audit_signing_cert_subject_name "$TPS_AUDIT_SIGNING_CERT_SUBJECT_NAME" \
        -tps_audit_signing_cert_nickname "auditSigningCert cert-pki-tps" \
        -ca_hostname "$HOSTNAME" \
        -ca_port 8080 \
        -ca_ssl_port 8443 \
        -ca_admin_port 8443 \
        -drm_hostname "$HOSTNAME" \
        -drm_ssl_port 12443 \
        -admin_user "$TPS_ADMIN_USER" \
        -agent_name "$TPS_ADMIN_NAME" \
        -admin_email "$TPS_ADMIN_EMAIL" \
        -admin_password "$TPS_ADMIN_PASSWORD" \
        -agent_key_size 2048 \
        -agent_key_type rsa \
        -agent_cert_subject "$TPS_ADMIN_CERT_SUBJECT"

echo $PASSWORD > "$CERTS/password.txt"
PKCS12Export -d "$CERTS" -o "$CERTS/tps-client-certs.p12" -p "$CERTS/password.txt" -w "$CERTS/password.txt"

echo systemctl restart pki-tpsd@$TPS_INSTANCE_NAME.service
