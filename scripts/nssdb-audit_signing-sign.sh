#!/bin/sh

AKID="`cat nssdb/ca_signing.skid`"
echo "AKID: ${AKID}"

OCSP="`cat nssdb/ocsp_url`"
echo "OCSP: ${OCSP}"

echo -e "y\n${AKID}\n\n\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/audit_signing.csr \
 -o nssdb/audit_signing.crt \
 -c "ca_signing" \
 -3 \
 --extAIA \
 --keyUsage critical,digitalSignature,nonRepudiation

certutil -A -d nssdb -n "audit_signing" -i nssdb/audit_signing.crt -t ",,P"

openssl x509 -text -noout -in nssdb/audit_signing.crt
