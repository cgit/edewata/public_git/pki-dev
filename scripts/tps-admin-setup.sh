#!/bin/sh -x

USERNAME=tpsadmin
#USERNAME=admin

#pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-add $USERNAME --fullName "TPS Administrator"
#pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-group-member-add "Administrators" $USERNAME

REQUEST_ID=`pki -c Secret.123 client-cert-request uid=$USERNAME | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-cert-add $USERNAME --serial $CERT_ID
pki -c Secret.123 client-cert-import $USERNAME --serial $CERT_ID

pki -c Secret.123 client-cert-show $USERNAME --pkcs12 $USERNAME.p12 --pkcs12-password Secret.123
