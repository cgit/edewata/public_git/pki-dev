#!/bin/sh -x

cat > ca-tomcat8.cfg <<EOF
[DEFAULT]
pki_backup_keys=True
pki_backup_password=Secret.123
pki_pin=Secret.123

[Tomcat]
tomcat_home=/usr/share/tomcat80

[CA]
pki_admin_email=caadmin@example.com
pki_admin_name=caadmin
pki_admin_nickname=caadmin
pki_admin_password=Secret.123
pki_admin_uid=caadmin

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=ca,dc=pki,dc=example,dc=com
pki_ds_password=Secret.123
pki_ds_database=ca

pki_security_domain_name=EXAMPLE

#pki_server_pkcs12_path=pki-server.p12
#pki_server_pkcs12_password=Secret.123

pki_ca_signing_nickname=ca_signing
pki_ocsp_signing_nickname=ca_ocsp_signing
pki_audit_signing_nickname=ca_audit_signing
pki_ssl_server_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -vv -f ca-tomcat8.cfg -s CA
