#!/bin/sh

mkdir -p tmp

cat > tmp/subsystem-cmc-request.cfg << EOF
# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default is internal).
tokenname=internal

# Nickname for agent certificate.
nickname=caadmin

# Request format: pkcs10 or crmf.
format=pkcs10

# Total number of PKCS10/CRMF requests.
numRequests=1

# Path to the PKCS10/CRMF request.
# The content must be in Base-64 encoded format.
# Multiple files are supported. They must be separated by space.
input=$PWD/tmp/subsystem.csr

# Path for the CMC request in binary format
output=$PWD/tmp/subsystem-cmc-request.bin
EOF

CMCRequest tmp/subsystem-cmc-request.cfg

cat > tmp/subsystem-cmc-submit.cfg << EOF
# PKI server host name.
host=$HOSTNAME

# PKI server port number.
port=8443

# Use secure connection.
# For secure connection with ECC, set environment variable 'export NSS_USE_DECODED_CKA_EC_POINT=1'.
secure=true

# Use client authentication.
clientmode=true

# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default: internal).
tokenname=internal

# Nickname of agent certificate.
nickname=caadmin

# CMC servlet path
#servlet=/ca/ee/ca/profileSubmitCMCFullSubsystemCert
servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caCMCsubsystemCert

# Path for the CMC request.
input=tmp/subsystem-cmc-request.bin

# Path for the CMC response.
output=tmp/subsystem-cmc-response.bin
EOF

HttpClient tmp/subsystem-cmc-submit.cfg

CMCResponse -d ~/.dogtag/nssdb -i tmp/subsystem-cmc-response.bin -o tmp/subsystem.crt

#BtoA tmp/subsystem-cmc-response.bin tmp/subsystem-cmc-response.b64
#echo "-----BEGIN PKCS7-----" > tmp/subsystem.crt
#cat tmp/subsystem-cmc-response.b64 >> tmp/subsystem.crt
#echo "-----END PKCS7-----" >> tmp/subsystem.crt
