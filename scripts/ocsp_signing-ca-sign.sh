#!/bin/sh

CMD="pki ca-cert-request-submit --profile caOCSPCert --csr-file tmp/ocsp_signing.csr"
echo $CMD
REQUEST_ID=`$CMD | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CMD="pki -c Secret.123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID"
echo $CMD
CERT_ID=`$CMD | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki cert-show --output tmp/ocsp_signing.crt $CERT_ID

