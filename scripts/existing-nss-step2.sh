#!/bin/sh -x

/bin/cp -f ca_signing.csr /tmp
/bin/cp -f internal.txt /tmp
/bin/cp -f nssdb.tar.gz /tmp

sed -i "s/internal=.*/internal=`cat /tmp/internal.txt`/" /var/lib/pki/pki-tomcat/conf/password.conf

tar xvf /tmp/nssdb.tar.gz -C /var/lib/pki/pki-tomcat/alias
#certutil -F -d /var/lib/pki/pki-tomcat/alias -f /tmp/internal.txt -n "Server-Cert cert-pki-tomcat" 
certutil -F -d /var/lib/pki/pki-tomcat/alias -f /tmp/internal.txt -n "subsystemCert cert-pki-tomcat"
certutil -F -d /var/lib/pki/pki-tomcat/alias -f /tmp/internal.txt -n "ocspSigningCert cert-pki-tomcat CA"
certutil -F -d /var/lib/pki/pki-tomcat/alias -f /tmp/internal.txt -n "auditSigningCert cert-pki-tomcat CA"

pkispawn -v -f existing-nss-step2.cfg -s CA
