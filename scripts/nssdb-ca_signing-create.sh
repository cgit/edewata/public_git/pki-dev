#!/bin/sh

SKID="0x`openssl rand -hex 20`"
echo $SKID > nssdb/ca_signing.skid

OCSP="http://$HOSTNAME:8080/ca/ocsp"
echo $OCSP > nssdb/ocsp_url

echo -e "y\n\ny\ny\n${SKID}\n\n\n\n${SKID}\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -S \
 -x \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -n "ca_signing" \
 -s "CN=CA Signing Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -t "CT,C,C" \
 -m $RANDOM \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 -2 \
 -3 \
 --extAIA \
 --extSKID \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation
