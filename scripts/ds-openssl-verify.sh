#!/bin/sh -x

LDAPTLS_CACERT=ds.crt \
   ldapsearch -H ldaps://$HOSTNAME:636 -x -D "cn=Directory Manager" -w Secret123 \
   -b "dc=example,dc=com" -s base "(objectClass=*)"

LDAPTLS_CACERTDIR=/etc/dirsrv/slapd-pki-tomcat \
   ldapsearch -H ldaps://$HOSTNAME:636 \
   -x -D "cn=Directory Manager" -w Secret123 \
   -b "dc=example,dc=com" -s base "(objectClass=*)"

