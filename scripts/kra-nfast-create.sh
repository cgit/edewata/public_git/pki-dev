#!/bin/sh -x

mkdir -p tmp

CA_HOSTNAME=`cat tmp/ca.hostname`
#USER=`cat user.txt`
TOKEN=softcard

cat > tmp/kra-nfast.cfg << EOF
[DEFAULT]
pki_pin=Secret.123
pki_hsm_enable=True

pki_hsm_libfile=/opt/nfast/toolkits/pkcs11/libcknfast.so
pki_hsm_modulename=nfast
pki_token_name=$TOKEN
pki_token_password=Secret.123
pki_pin=Secret.123

[KRA]
pki_admin_email=kraadmin@example.com
pki_admin_name=kraadmin
pki_admin_nickname=kraadmin
pki_admin_password=Secret.123
pki_admin_uid=kraadmin

pki_client_database_password=Secret.123
pki_client_pkcs12_password=Secret.123
pki_client_database_purge=False

pki_ds_base_dn=dc=kra,dc=pki,dc=example,dc=com
pki_ds_database=kra
pki_ds_password=Secret.123

pki_security_domain_hostname=$CA_HOSTNAME
pki_security_domain_name=EXAMPLE
pki_security_domain_user=caadmin
pki_security_domain_password=Secret.123

pki_storage_nickname=kra_storage
#pki_storage_nickname=$USER/%(pki_instance_name)s/kra_storage
#pki_storage_token=internal
pki_storage_token=$TOKEN

pki_transport_nickname=kra_transport
#pki_transport_nickname=$USER/%(pki_instance_name)s/kra_transport
#pki_transport_token=internal
pki_transport_token=$TOKEN

pki_audit_signing_nickname=kra_audit_signing
#pki_audit_signing_nickname=$USER/%(pki_instance_name)s/kra_audit_signing
#pki_audit_signing_token=internal
pki_audit_signing_token=$TOKEN

pki_sslserver_nickname=sslserver
#pki_sslserver_nickname=$USER/%(pki_instance_name)s/sslserver/%(pki_hostname)s
#pki_sslserver_token=internal
pki_sslserver_token=$TOKEN

pki_subsystem_nickname=subsystem
#pki_subsystem_nickname=$USER/%(pki_instance_name)s/subsystem
#pki_subsystem_token=internal
pki_subsystem_token=$TOKEN
EOF

pkispawn -f tmp/kra-nfast.cfg -s KRA
