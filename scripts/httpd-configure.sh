#!/bin/sh -x

cat > /etc/httpd/conf.d/tet.conf << EOF
Alias /pki/repo/ "/root/CS/repo/"

<Directory "/root/CS/repo">
    Options Indexes MultiViews FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>

Alias /pki/test/ "/root/CS/tet/"

<Directory "/root/CS/tet">
    Options Indexes MultiViews FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>
EOF
