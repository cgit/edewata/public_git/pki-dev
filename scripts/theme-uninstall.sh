#!/bin/sh -x

yum erase -y\
	dogtag-pki-ca-theme\
	dogtag-pki-kra-theme\
	dogtag-pki-tps-theme\
	dogtag-pki-tks-theme\
	dogtag-pki-console-theme\
	dogtag-pki-ra-theme\
	dogtag-pki-server-theme\
	dogtag-pki-common-theme\
	dogtag-pki-ocsp-theme\
	redhat-pki-server-theme\
	redhat-pki-console-theme
