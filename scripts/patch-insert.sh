#!/bin/sh

if [ $# != 1 ]; then
    echo "usage: patch-insert.sh <number>"
    exit 1
fi

NUMBER=$1

cd ../../shared

for filename in `ls -r pki-$USER-*.patch`;do
    oldnumber=`echo $filename | sed 's/^[^-]*-[^-]*-\([^-]*\)-.*/\1/'`

    if [ $oldnumber -lt $NUMBER ]; then
        exit
    fi

    newnumber=`expr $oldnumber + 1`

    while [ `expr length $newnumber` -lt 4 ]; do
        newnumber=0$newnumber
    done

    newfilename=`echo $filename | sed "s/^\([^-]*-[^-]*\)-$oldnumber-/\1-$newnumber-/"`

    echo Renaming patch $oldnumber to $newnumber.
    mv $filename $newfilename
done
