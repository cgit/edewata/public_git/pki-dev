#!/bin/sh

mkdir -p tmp

cat > tmp/testuser-cmc-request.cfg << EOF
# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default is internal).
tokenname=internal

# Nickname for agent certificate.
nickname=caadmin

# Request format: pkcs10 or crmf.
format=pkcs10

# Total number of PKCS10/CRMF requests.
numRequests=1

# Path to the PKCS10/CRMF request.
# The content must be in Base-64 encoded format.
# Multiple files are supported. They must be separated by space.
input=tmp/testuser.csr

# Path for the CMC request.
output=tmp/testuser-cmc-request.bin
EOF

CMCRequest tmp/testuser-cmc-request.cfg

cat > tmp/testuser-cmc-submit.cfg << EOF
# PKI server host name.
host=$HOSTNAME

# PKI server port number.
port=8443

# Use secure connection.
# For secure connection with ECC, set environment variable 'export NSS_USE_DECODED_CKA_EC_POINT=1'.
secure=true

# Use client authentication.
clientmode=true

# NSS database directory.
dbdir=$HOME/.dogtag/nssdb

# NSS database password.
password=Secret.123

# Token name (default: internal).
tokenname=internal

# Nickname of agent certificate.
nickname=caadmin

# CMC servlet path
#servlet=/ca/ee/ca/profileSubmitCMCFull
#servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caFullCMCUserSignedCert
servlet=/ca/ee/ca/profileSubmitCMCFull?profileId=caFullCMCUserCert

# Path for the CMC request.
input=tmp/testuser-cmc-request.bin

# Path for the CMC response.
output=tmp/testuser-cmc-response.bin
EOF

HttpClient tmp/testuser-cmc-submit.cfg

CMCResponse -d ~/.dogtag/nssdb -i tmp/testuser-cmc-response.bin -o tmp/testuser.crt

