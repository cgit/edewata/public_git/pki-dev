#!/bin/sh

CS_CFG=/var/lib/pki/pki-tomcat/tps/conf/CS.cfg

mkdir -p ~/tps/profiles/orig
cd ~/tps/profiles/orig

list=`grep target.Profiles.list= /var/lib/pki/pki-tomcat/tps/conf/CS.cfg | sed -e 's/.*=//' | sed -e 's/,/ /g'`
for name in $list; do
    echo Exporting $name.cfg
    cat $CS_CFG | grep "op\..*\.$name\..*" > $name.cfg
done
