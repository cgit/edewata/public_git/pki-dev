#!/bin/sh -x

setup-ds.pl --silent --\
	General.FullMachineName=$HOSTNAME\
	General.SuiteSpotUserID=nobody\
	General.SuiteSpotGroup=nobody\
	slapd.ServerPort=10389\
	slapd.ServerIdentifier=pki-clone\
	slapd.Suffix=dc=example,dc=com\
	slapd.RootDN="cn=Directory Manager"\
	slapd.RootDNPwd=Secret123\

ldapadd -h $HOSTNAME -p 10389 -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: dc=pki,dc=example,dc=com
objectClass: domain
dc: pki
EOF
