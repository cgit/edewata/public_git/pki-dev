#!/bin/sh -x

INSTANCE_NAME=kra-master

java -agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n -classpath :/usr/share/tomcat/bin/bootstrap.jar:/usr/share/tomcat/bin/tomcat-juli.jar:/usr/share/java/commons-daemon.jar -Dcatalina.base=/var/lib/pki/$INSTANCE_NAME -Dcatalina.home=/usr/share/tomcat -Djava.endorsed.dirs= -Djava.io.tmpdir=/var/lib/pki/$INSTANCE_NAME/temp -Djava.util.logging.config.file=/var/lib/pki/$INSTANCE_NAME/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager org.apache.catalina.startup.Bootstrap start
