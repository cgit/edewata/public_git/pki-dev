#!/bin/sh

################################################################################
# CA Level 1
################################################################################

LEVEL1_SKID="0x`openssl rand -hex 20`"
OCSP="http://$HOSTNAME:8080/ca/ocsp"

echo -e "y\n\ny\ny\n${LEVEL1_SKID}\n\n\n\n${LEVEL1_SKID}\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -S \
 -x \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -n "level1" \
 -s "CN=Level 1 CA Signing Certificate,O=EXAMPLE" \
 -t "CT,C,C" \
 -m $RANDOM \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 -2 \
 -3 \
 --extAIA \
 --extSKID \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

certutil -L -d nssdb -n "level1" -a > nssdb/level1.crt

################################################################################
# CA Level 2
################################################################################

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=Level 2 CA Signing Certificate,O=EXAMPLE" \
 -o nssdb/level2.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 -2 \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

openssl req -inform der -in nssdb/level2.csr.der -out nssdb/level2.csr

LEVEL2_SKID="0x`openssl rand -hex 20`"

echo -e "y\n\ny\ny\n${LEVEL1_SKID}\n\n\n\n${LEVEL2_SKID}\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/level2.csr \
 -o nssdb/level2.crt \
 -c "level1" \
 -2 \
 -3 \
 --extAIA \
 --extSKID \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

certutil -A -d nssdb -n "level2" -i nssdb/level2.crt -t "CT,C,C"

################################################################################
# CA Level 3
################################################################################

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=Level 3 CA Signing Certificate,O=EXAMPLE" \
 -o nssdb/level3.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 -2 \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

openssl req -inform der -in nssdb/level3.csr.der -out nssdb/level3.csr

LEVEL3_SKID="0x`openssl rand -hex 20`"

echo -e "y\n\ny\ny\n${LEVEL2_SKID}\n\n\n\n${LEVEL3_SKID}\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/level3.csr \
 -o nssdb/level3.crt \
 -c "level2" \
 -2 \
 -3 \
 --extAIA \
 --extSKID \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

certutil -A -d nssdb -n "level3" -i nssdb/level3.crt -t "CT,C,C"

################################################################################
# Cert Chain
################################################################################

# complete chain
openssl crl2pkcs7 -nocrl -certfile nssdb/level1.crt -certfile nssdb/level2.crt -certfile nssdb/level3.crt -out nssdb/cert_chain.p7b

# out of order chain
#openssl crl2pkcs7 -nocrl -certfile nssdb/level2.crt -certfile nssdb/level3.crt -certfile nssdb/level1.crt -out nssdb/cert_chain.p7b

# root cert only
#openssl crl2pkcs7 -nocrl -certfile nssdb/level1.crt -out nssdb/cert_chain.p7b

# leaf cert only
#openssl crl2pkcs7 -nocrl -certfile nssdb/level3.crt -out nssdb/cert_chain.p7b

# broken chain
#openssl crl2pkcs7 -nocrl -certfile nssdb/level3.crt -certfile nssdb/level1.crt -out nssdb/cert_chain.p7b

# duplicate cert
#openssl crl2pkcs7 -nocrl -certfile nssdb/level1.crt -certfile nssdb/level2.crt -certfile nssdb/level2.crt -out nssdb/cert_chain.p7b

openssl pkcs7 -print_certs -in nssdb/cert_chain.p7b
