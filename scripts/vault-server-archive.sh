#!/bin/python

import getopt
import sys

import pki
import pki.client
import pki.crypto
import pki.key
import pki.kra
import pki.systemcert

def usage():
    print "usage: vault-server-archive --user-id <user ID> --secret-id <secret ID> --data <data>"

def main(argv):

    try:
        opts, _ = getopt.getopt(argv[1:], 'c:d:hv', [
            'user-id=', 'secret-id=', 'data=',
            'verbose', 'help'])

    except getopt.GetoptError as e:
        print 'ERROR: ' + str(e)
        usage()
        sys.exit(1)

    verbose = False

    nssdb_directory = "/root/.dogtag/pki-tomcat/ca/alias"
    nssdb_password = "Secret123"

    transport_cert_nickname = "KRA Transport Certificate"
    admin_cert = "/root/.dogtag/pki-tomcat/ca_admin_cert.pem"

    scheme = 'https'
    host = 'localhost'
    port = '8443'
    subsystem = 'kra'

    user_id = None
    secret_id = None
    data = None

    for o, a in opts:
        if o == '-v':
            verbose = True

        elif o == '-d':
            nssdb_directory = a

        elif o == '-c':
            nssdb_password = a

        elif o == '--user-id':
            user_id = a

        elif o == '--secret-id':
            secret_id = a

        elif o == '--data':
            data = a

    if user_id is None or secret_id is None or data is None:
        usage()
        sys.exit(1)

    client_key_id = "%s:%s" % (user_id, secret_id)
    if verbose:
        print "Client Key ID: " + client_key_id

    crypto = pki.crypto.NSSCryptoProvider(nssdb_directory, nssdb_password)
    crypto.initialize()

    conn = pki.client.PKIConnection(scheme, host, port, subsystem)
    conn.set_authentication_cert(admin_cert)

    kra_client = pki.kra.KRAClient(conn, crypto, transport_cert_nickname)
    key_client = kra_client.keys

    response = key_client.archive_key(
        client_key_id,
        pki.key.KeyClient.PASS_PHRASE_TYPE,
        data)

    if verbose:
        print "Key ID: " + str(response.get_key_id())

if __name__ == '__main__':
    main(sys.argv)
