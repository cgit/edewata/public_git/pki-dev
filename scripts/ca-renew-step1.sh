#!/bin/sh -x

timedatectl set-ntp true --adjust-system-clock

./pki-nuke.sh pki-tomcat

./ca-create.sh

pki-server subsystem-cert-find ca
certutil -L -d /var/lib/pki/pki-tomcat/alias
