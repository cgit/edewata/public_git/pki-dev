#!/bin/sh

password="`cat lunasa.txt`"

for id in "$@"
do
    echo "Deleting id $id..."

    /usr/safenet/lunaclient/bin/cmu list -display handle,class -id $id -password $password | while read cert
    do
        class=$(echo $cert | cut -d' ' -f2 | cut -d= -f2)
        handle=$(echo $cert | cut -d' ' -f1 | cut -d= -f2)
        echo " - $class: $handle"

        /usr/safenet/lunaclient/bin/cmu delete -handle $handle -force -password $password
    done
done
