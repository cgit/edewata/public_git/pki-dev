#!/bin/sh -x

rpm -e --nodeps pki-symkey
rpm -e --nodeps pki-base
rpm -e --nodeps pki-base-java
rpm -e --nodeps python3-pki
rpm -e --nodeps pki-tools
rpm -e --nodeps pki-server
rpm -e --nodeps pki-ca
rpm -e --nodeps pki-kra
rpm -e --nodeps pki-ocsp
rpm -e --nodeps pki-tks
rpm -e --nodeps pki-tps
rpm -e --nodeps pki-core-debuginfo
rpm -e --nodeps pki-util
rpm -e --nodeps pki-silent
rpm -e --nodeps pki-selinux
rpm -e --nodeps pki-javadoc
rpm -e --nodeps pki-base-python3
