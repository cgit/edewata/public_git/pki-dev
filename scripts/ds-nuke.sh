#!/bin/sh

INSTANCE_NAME=$1

if [ "$INSTANCE_NAME" == "" ]; then
    echo "usage: ds-nuke.sh <instance name>"
    exit 1
fi

echo "Deleting instance $INSTANCE_NAME"

rm -rf /etc/dirsrv/slapd-$INSTANCE_NAME
rm -rf /etc/dirsrv/slapd-$INSTANCE_NAME.removed
rm -rf /var/lock/dirsrv/slapd-$INSTANCE_NAME
rm -rf /var/log/dirsrv/slapd-$INSTANCE_NAME
