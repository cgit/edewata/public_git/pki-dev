#!/bin/sh -x

cp password.txt /etc/dirsrv/slapd-pki-tomcat/password.txt
chown nobody.nobody /etc/dirsrv/slapd-pki-tomcat/password.txt
chmod 400 /etc/dirsrv/slapd-pki-tomcat/password.txt
echo "Internal (Software) Token:`cat /etc/dirsrv/slapd-pki-tomcat/password.txt`" > /etc/dirsrv/slapd-pki-tomcat/pin.txt
chown nobody.nobody /etc/dirsrv/slapd-pki-tomcat/pin.txt
chmod 400 /etc/dirsrv/slapd-pki-tomcat/pin.txt

# generate CA certificate
#openssl req -newkey rsa:2048 -keyout dsca.key -nodes -x509 -out dsca.pem -subj "/CN=CAcert" -days 365
#openssl pkcs12 -export -in dsca.pem -inkey dsca.key -out dsca.p12 -name "CA certificate" -passout pass:Secret123
#pk12util -i dsca.p12 -d /etc/dirsrv/slapd-pki-tomcat -k /etc/dirsrv/slapd-pki-tomcat/pwdfile.txt -W Secret123
#certutil -M -d /etc/dirsrv/slapd-pki-tomcat -n "CA certificate" -t "CTu,u,u"

# generate server certificate
#openssl req -newkey rsa:2048 -keyout ds.key -nodes -new -out ds.csr -subj "/CN=$HOSTNAME" -days 365
#openssl x509 -req -in ds.csr -CA dsca.pem -CAkey dsca.key -CAcreateserial -out ds.pem
#openssl pkcs12 -export -in ds.pem -inkey ds.key -out ds.p12 -name "Server-Cert" -passout pass:Secret123
#pk12util -i ds.p12 -d /etc/dirsrv/slapd-pki-tomcat -k /etc/dirsrv/slapd-pki-tomcat/pwdfile.txt -W Secret123

openssl req -newkey rsa:2048 -keyout ds.key -nodes -x509 -out ds.crt -subj "/CN=$HOSTNAME" -days 365
openssl pkcs12 -export -in ds.crt -inkey ds.key -out ds.p12 -name "DS Certificate" -passout file:/etc/dirsrv/slapd-pki-tomcat/password.txt
pk12util -i ds.p12 -d /etc/dirsrv/slapd-pki-tomcat -k /etc/dirsrv/slapd-pki-tomcat/password.txt -w /etc/dirsrv/slapd-pki-tomcat/password.txt
certutil -M -d /etc/dirsrv/slapd-pki-tomcat -n "DS Certificate" -t "CT,C,C"
