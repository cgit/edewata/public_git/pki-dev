#!/bin/sh -x

rm -rf /tmp/ca.p12
rm -rf /tmp/external.crt
rm -rf /tmp/ca_signing.csr
rm -rf /tmp/ca_ocsp_signing.csr
rm -rf /tmp/ca_audit_signing.csr
rm -rf /tmp/sslserver.csr
rm -rf /tmp/subsystem.csr

/bin/cp ca.p12 /tmp
/bin/cp external.crt /tmp
/bin/cp ca_signing.csr /tmp
/bin/cp ca_ocsp_signing.csr /tmp
/bin/cp ca_audit_signing.csr /tmp
/bin/cp sslserver.csr /tmp
/bin/cp subsystem.csr /tmp

#/bin/cp -f ca_signing.csr /tmp
#/bin/cp -f ca_signing.p12 /tmp
#/bin/cp -f cert_chain.p7b /tmp

pkispawn -v -f existing-step2-p12.cfg -s CA
