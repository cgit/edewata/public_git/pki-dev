#!/bin/sh -x

setup-ds.pl --silent -- \
	General.FullMachineName=$HOSTNAME \
	General.SuiteSpotUserID=nobody \
	General.SuiteSpotGroup=nobody \
	slapd.ServerPort=389 \
	slapd.ServerIdentifier=pki-tomcat \
	slapd.Suffix=dc=example,dc=com \
	"slapd.RootDN=cn=Directory Manager" \
	slapd.RootDNPwd=Secret.123

ldapadd -h $HOSTNAME -x -D "cn=Directory Manager" -w Secret.123 << EOF
dn: dc=pki,dc=example,dc=com
objectClass: domain
dc: pki
EOF
