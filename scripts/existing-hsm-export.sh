#!/bin/sh -x

rm -rf ca_signing.csr
rm -rf ca_ocsp_signing.csr
rm -rf ca_audit_signing.csr
rm -rf subsystem.csr
rm -rf sslserver.csr


certutil -L -d /var/lib/pki/pki-tomcat/alias -n "edewata/pki-tomcat/ca_signing" -a > ca_signing.crt
#certutil -L -d /var/lib/pki/pki-tomcat/alias -n "edewata/pki-tomcat/ca_ocsp_signing" -a > ca_ocsp_signing.crt
#certutil -L -d /var/lib/pki/pki-tomcat/alias -n "edewata/pki-tomcat/ca_audit_signing" -a > ca_audit_signing.crt
#certutil -L -d /var/lib/pki/pki-tomcat/alias -n "edewata/pki-tomcat/subsystem" -a > subsystem.crt
#certutil -L -d /var/lib/pki/pki-tomcat/alias -n "edewata/pki-tomcat/sslserver" -a > sslserver.crt

echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_signing.csr
sed -n "/^ca.signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> ca_signing.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_signing.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_ocsp_signing.csr
#sed -n "/^ca.ocsp_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> ca_ocsp_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_ocsp_signing.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > ca_audit_signing.csr
#sed -n "/^ca.audit_signing.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> ca_audit_signing.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> ca_audit_signing.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > subsystem.csr
#sed -n "/^ca.subsystem.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> subsystem.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> subsystem.csr

#echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > sslserver.csr
#sed -n "/^ca.sslserver.certreq=/ s/^[^=]*=// p" < /var/lib/pki/pki-tomcat/ca/conf/CS.cfg >> sslserver.csr
#echo "-----END NEW CERTIFICATE REQUEST-----" >> sslserver.csr
