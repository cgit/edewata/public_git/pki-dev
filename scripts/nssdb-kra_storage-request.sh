#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=DRM Storage Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/kra_storage.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,dataEncipherment,keyEncipherment,digitalSignature,nonRepudiation \
 --extKeyUsage clientAuth

openssl req -inform der -in nssdb/kra_storage.csr.der -out nssdb/kra_storage.csr
