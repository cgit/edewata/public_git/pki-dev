#!/bin/sh -x

cp ca_signing.crt /tmp
cp external.crt /tmp
cp cert_chain.p7b /tmp

pkispawn -vv -f ca-level3-step2.cfg -s CA
