#!/bin/sh

/usr/bin/openssl ca \
 -batch \
 -passin pass:Secret123 \
 -notext \
 -config /root/.TinyCA/RootCA/openssl.cnf \
 -name ca_ca \
 -in "ca_signing.csr" \
 -out "ca_signing.crt" \
 -days 7200 \
 -preserveDN \
 -md sha256 \
 -noemailDN

# -config external-tinyca.cnf \

/bin/cp /root/.TinyCA/RootCA/cacert.pem external.crt
