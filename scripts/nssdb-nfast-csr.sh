#!/bin/sh

OUTPUT=ca_signing.csr

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -h edewata \
 -f password.txt \
 -s "CN=CA Signing Certificate,O=EXAMPLE" \
 -z nssdb/noise.bin \
 -k rsa \
 -g 2048 \
 -Z SHA512 \
 -2 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 -o ca.csr.der

BtoA ca.csr.der ca.csr.pem
echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > $OUTPUT
cat ca.csr.pem >> $OUTPUT
echo "-----END NEW CERTIFICATE REQUEST-----" >> $OUTPUT

rm ca.csr.der
rm ca.csr.pem
