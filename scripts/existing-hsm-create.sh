#!/bin/sh -x

rm -rf /tmp/ca_signing.csr
rm -rf /tmp/ca_signing.crt
rm -rf /tmp/ca_ocsp_signing.csr
rm -rf /tmp/ca_ocsp_signing.crt
rm -rf /tmp/ca_audit_signing.csr
rm -rf /tmp/ca_audit_signing.crt
rm -rf /tmp/subsystem.csr
rm -rf /tmp/subsystem.crt
rm -rf /tmp/sslserver.csr
rm -rf /tmp/sslserver.crt
rm -rf /tmp/external.crt

/bin/cp ca_signing.csr /tmp
/bin/cp ca_signing.crt /tmp
/bin/cp ca_ocsp_signing.csr /tmp
/bin/cp ca_ocsp_signing.crt /tmp
/bin/cp ca_audit_signing.csr /tmp
/bin/cp ca_audit_signing.crt /tmp
/bin/cp subsystem.csr /tmp
/bin/cp subsystem.crt /tmp
/bin/cp sslserver.csr /tmp
/bin/cp sslserver.crt /tmp
/bin/cp external.crt /tmp

pkispawn -v -f existing-hsm.cfg -s CA
