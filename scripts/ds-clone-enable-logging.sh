#!/bin/sh

ldapmodify -x -h localhost -p 390 -D "cn=Directory Manager" -w "Secret123" <<EOF
dn: cn=config
changetype: modify
replace: nsslapd-accesslog-logging-enabled
nsslapd-accesslog-logging-enabled: on
-
replace: nsslapd-accesslog-logbuffering
nsslapd-accesslog-logbuffering: off

EOF
