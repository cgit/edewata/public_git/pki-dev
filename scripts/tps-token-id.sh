#!/bin/sh

echo `hexdump -v -n "10" -e '1/1 "%02x"' /dev/urandom` | tr '[:lower:]'  '[:upper:]'
