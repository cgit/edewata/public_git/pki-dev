#!/bin/sh

#pki cert-show 0x1 --output tmp/external.crt
#openssl crl2pkcs7 -nocrl -certfile tmp/external.crt -out tmp/cert_chain.p7b
#pki -d /etc/pki/pki-tomcat/alias -c Secret.123 client-cert-show ca_signing --cert tmp/external.crt
pki-server cert-export ca_signing --cert-file tmp/external.crt

./ca_signing-ca-sign.sh

openssl crl2pkcs7 -nocrl \
 -certfile tmp/external.crt \
 -certfile tmp/ca_signing.crt \
 -out tmp/ca_signing.p7b
