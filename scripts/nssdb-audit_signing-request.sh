#!/bin/sh

certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=Audit Signing Certificate,OU=pki-tomcat,O=EXAMPLE" \
 -o nssdb/audit_signing.csr.der \
 -k rsa \
 -g 2048 \
 -Z SHA256 \
 --keyUsage critical,digitalSignature,nonRepudiation

openssl req -inform der -in nssdb/audit_signing.csr.der -out nssdb/audit_signing.csr
