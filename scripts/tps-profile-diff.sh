#!/bin/sh

ORIG_DIR=~/tps/profiles/orig
NEW_DIR=~/tps/profiles/new

list=`cd $ORIG_DIR; ls`
for name in $list; do
    echo Comparing $name
    diff  $ORIG_DIR/$name $NEW_DIR/$name
done
