#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

INSTANCE_NAME=pki-tomcat
SERVER_CERT_DIR=/var/lib/pki/$INSTANCE_NAME
CLIENT_CERT_DIR=~/.dogtag/$INSTANCE_NAME
CERT_NAME="transportCert cert-$INSTANCE_NAME KRA"

# export admin certificate
rm -rf /tmp/auth.pem
openssl pkcs12 -in $CLIENT_CERT_DIR/ca_admin_cert.p12 -out /tmp/auth.pem -nodes -passin pass:Secret123

# create client database
rm -rf /tmp/drmtest-certdb
mkdir -p /tmp/drmtest-certdb
certutil -N -d /tmp/drmtest-certdb -f $CLIENT_CERT_DIR/ca/password.conf
chmod -R +r /tmp/drmtest-certdb

# export transport certificate
#certutil -L -d $SERVER_CERT_DIR/alias -n "$CERT_NAME" -a > transport.pem

# import transport certificate
#certutil -A -d /tmp/drmtest-certdb -n "kra transport cert" -i transport.pem -a -t "u,u,u"

# run KRA test
cd $SRC_DIR/pki/base/kra/functional
python drmtest.py
