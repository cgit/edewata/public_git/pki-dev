#!/bin/sh

TPSHOST=`cat tps.host`

ldapmodify -x -D "cn=Directory Manager" -w Secret123 -c << EOF
dn: cn=Token Key Service Manager Agents,ou=groups,dc=tks,dc=pki,dc=example,dc=com
changetype: modify
delete: uniqueMember
uniqueMember: uid=TPS-$TPSHOST-8443,ou=people,dc=tks,dc=pki,dc=example,dc=com

dn: uid=TPS-$TPSHOST-8443,ou=people,dc=tks,dc=pki,dc=example,dc=com
changetype: delete
EOF
