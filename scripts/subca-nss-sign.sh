#!/bin/sh

rm -rf tmp/external
mkdir -p tmp/external
certutil -N -d tmp/external -f password.txt
openssl rand -out tmp/external/noise.bin 2048

echo "## Generating external CA certificate..."

ROOTCA_SKID="0x`openssl rand -hex 20`"

echo -e "y\n\ny\n${ROOTCA_SKID}\n\n" | \
 certutil -S \
 -d tmp/external \
 -f password.txt \
 -z tmp/external/noise.bin \
 -n "External CA" \
 -s "CN=External CA,O=EXTERNAL" \
 -x \
 -t "CTu,Cu,Cu" \
 -m $RANDOM\
 -2 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 --extSKID

# --nsCertType sslCA,smimeCA,objectSigningCA

echo "## Exporting external CA certificate..."

certutil -L -d tmp/external -n "External CA" -a > tmp/external.crt

echo "## Signing the CA signing certificate..."

SUBCA_SKID="0x`openssl rand -hex 20`"
SUBCA_OCSP="http://$HOSTNAME:8080/ca/ocsp"

echo -e "y\n\ny\ny\n${ROOTCA_SKID}\n\n\n\n${SUBCA_SKID}\n\n2\n7\n${SUBCA_OCSP}\n\n\n\n" | \
 certutil -C \
 -d tmp/external \
 -f password.txt \
 -m $RANDOM \
 -a \
 -i tmp/ca_signing.csr \
 -o tmp/ca_signing.crt \
 -c "External CA" \
 --extSKID \
 -2 -3 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 --extAIA \
 --extSKID

echo "## Generating certificate chain..."

certutil -A -d tmp/external -n "CA Signing Certificate" -t "CT,C,C" -a -i tmp/ca_signing.crt

#openssl crl2pkcs7 -nocrl -certfile tmp/external.crt -out tmp/cert_chain.p7b
#openssl crl2pkcs7 -nocrl -certfile tmp/external.crt -certfile tmp/ca_signing.crt -out tmp/cert_chain.p7b

#certutil -C \
# -d tmp/external \
# -f password.txt \
# -m $RANDOM \
# -a \
# -i tmp/ca_signing.csr \
# -o tmp/ca_signing.crt \
# -c "External CA"
