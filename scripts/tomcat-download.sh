#!/bin/sh -x

mkdir -p ~/Downloads
cd ~/Downloads

#https://kojipkgs.fedoraproject.org/packages/pki-core/10.3.1/1.fc24/noarch/pki-base-10.3.1-1.fc24.noarch.rpm
BASE_URL=http://kojipkgs.fedoraproject.org/packages

PACKAGE=tomcat
VERSION=7.0.69
RELEASE=2
OS=el7

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/tomcat-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/tomcat-el-2.2-api-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/tomcat-jsp-2.2-api-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/tomcat-lib-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/tomcat-servlet-3.0-api-$VERSION-$RELEASE.$OS.noarch.rpm
