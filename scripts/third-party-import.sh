#!/bin/sh

certutil -A -d /var/lib/pki/pki-tomcat/alias -n "Third-party CA" -t "CT,C,C" -i third-party.crt
