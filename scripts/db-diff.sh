#!/bin/sh -x

BRANCH=$1
SUBSYSTEM=$2

cd ../../pki

git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/acl.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/database.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/db.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/index.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/manager.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/schema.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/vlv.ldif
git diff $BRANCH --follow -- base/$SUBSYSTEM/shared/conf/vlvtasks.ldif
