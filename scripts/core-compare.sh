#!/bin/sh

old=$1
new=/usr/share/java/pki

./jar-compare.sh pki-ca.jar $old $new
./jar-compare.sh pki-certsrv.jar $old $new
./jar-compare.sh pki-cmsbundle.jar $old $new
./jar-compare.sh pki-cmscore.jar $old $new
./jar-compare.sh pki-cms.jar $old $new
./jar-compare.sh pki-cmsutil.jar $old $new
./jar-compare.sh pki-kra.jar $old $new
./jar-compare.sh pki-nsutil.jar $old $new
./jar-compare.sh pki-ocsp.jar $old $new
./jar-compare.sh pki-tks.jar $old $new
./jar-compare.sh pki-tomcat.jar $old $new
./jar-compare.sh pki-tools.jar $old $new
./jar-compare.sh pki-tps.jar $old $new
