#!/bin/sh

INSTANCE_DIR=/var/lib/pki/tks-master
grep "internal=" $INSTANCE_DIR/conf/password.conf | sed "s/internal=//" > $INSTANCE_DIR/conf/internal.txt
tkstool -D -d $INSTANCE_DIR/alias -n sharedSecret -f $INSTANCE_DIR/conf/internal.txt
