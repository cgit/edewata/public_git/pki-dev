#!/bin/sh

SKID="0x`openssl rand -hex 20`"
echo $SKID > nssdb/ca_signing.skid

OCSP="http://$HOSTNAME:8080/ca/ocsp"
echo $OCSP > nssdb/ocsp_url

echo -e "y\n\ny\ny\n${SKID}\n\n\n\n${SKID}\n\n2\n7\n${OCSP}\n\n\n\n" | \
 certutil -C \
 -x \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i nssdb/ca_signing.csr \
 -o nssdb/ca_signing.crt \
 -2 \
 -3 \
 --extAIA \
 --extSKID \
 --keyUsage critical,certSigning,crlSigning,digitalSignature,nonRepudiation

certutil -A -d nssdb -n "ca_signing" -i nssdb/ca_signing.crt -t "CT,C,C"

openssl x509 -text -noout -in nssdb/ca_signing.crt
