#!/bin/sh -x

mkdir -p tmp

MASTER=`cat tmp/master.txt`

cat > tmp/kra-clone.cfg << EOF
[DEFAULT]
pki_pin=Secret.123

#pki_instance_name=pki-clone
#pki_http_port=18080
#pki_https_port=18443
#pki_ajp_port=18009
#pki_tomcat_server_port=18005

[KRA]
pki_admin_email=kraadmin@example.com
pki_admin_name=kraadmin
pki_admin_nickname=kraadmin
pki_admin_password=Secret.123
pki_admin_uid=kraadmin

#pki_backup_keys=True
pki_backup_password=Secret.123

pki_client_database_password=Secret.123
pki_client_database_purge=False
pki_client_pkcs12_password=Secret.123

pki_ds_base_dn=dc=kra,dc=pki,dc=example,dc=com
pki_ds_database=kra
pki_ds_password=Secret.123
#pki_ds_ldap_port=10389

pki_security_domain_hostname=$MASTER
pki_security_domain_https_port=8443
pki_security_domain_password=Secret.123
pki_security_domain_user=caadmin

pki_issuing_ca_hostname=$MASTER
#pki_issuing_ca_https_port=18443

pki_clone=True
pki_clone_pkcs12_password=Secret.123
pki_clone_pkcs12_path=$PWD/tmp/kra-certs.p12
pki_clone_replicate_schema=True
pki_clone_uri=https://$MASTER:8443

pki_storage_nickname=kra_storage
pki_transport_nickname=kra_transport
pki_audit_signing_nickname=kra_audit_signing
pki_sslserver_nickname=sslserver
pki_subsystem_nickname=subsystem
EOF

pkispawn -vvv -f tmp/kra-clone.cfg -s KRA
