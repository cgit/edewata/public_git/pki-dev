#!/bin/sh -x

setup-ds-admin.pl --silent --\
	General.FullMachineName=$HOSTNAME\
	General.SuiteSpotUserID=nobody\
	General.SuiteSpotGroup=nobody\
	General.AdminDomain=example.com\
	General.ConfigDirectoryAdminID=admin\
	General.ConfigDirectoryAdminPwd=Secret123\
	General.ConfigDirectoryLdapURL=ldap://$HOSTNAME:389/o=NetscapeRoot\
	slapd.ServerPort=389\
	slapd.ServerIdentifier=pki-tomcat\
	slapd.Suffix=dc=example,dc=com\
	slapd.RootDN="cn=Directory Manager"\
	slapd.RootDNPwd=Secret123\
	admin.Port=9830\
	admin.ServerAdminID=admin\
	admin.ServerAdminPwd=Secret123
