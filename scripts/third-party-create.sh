#!/bin/sh

rm -rf third
mkdir third
echo Secret123 > third/password.txt
certutil -N -d third -f third/password.txt
openssl rand -out third/noise.bin 2048

# generate external CA certificate

echo -e "y\n\ny\n" | \
 certutil -S \
 -d third \
 -f third/password.txt \
 -z third/noise.bin \
 -n "Third-party CA" \
 -s "CN=Third-party CA,O=OTHER" \
 -x \
 -t "CTu,Cu,Cu" \
 -m $RANDOM\
 -2 \
 --keyUsage certSigning \
 --nsCertType sslCA,smimeCA,objectSigningCA

certutil -L -d third -n "Third-party CA" -a > third-party.crt

openssl crl2pkcs7 -nocrl -certfile third-party.crt -out third-party.p7b
