#!/bin/sh -x

rm -rf /tmp/ca.p12
rm -rf /tmp/external.crt
rm -rf /tmp/ca_signing.csr
rm -rf /tmp/ca_ocsp_signing.csr
rm -rf /tmp/ca_audit_signing.csr
rm -rf /tmp/sslserver.csr
rm -rf /tmp/subsystem.csr

/bin/cp ca.p12 /tmp
/bin/cp external.crt /tmp
/bin/cp ca_signing.csr /tmp
/bin/cp ca_ocsp_signing.csr /tmp
/bin/cp ca_audit_signing.csr /tmp
/bin/cp sslserver.csr /tmp
/bin/cp subsystem.csr /tmp

pkispawn -v -f ca-p12.cfg -s CA
