#!/bin/sh

INPUT=$1

if [ "$INPUT" == "" ]; then
    INPUT=cert-request-caUserCert.xml
fi

# Submit request anonymously
pki cert-request-submit "$INPUT"
