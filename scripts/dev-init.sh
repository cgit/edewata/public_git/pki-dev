#!/bin/sh -x

dnf copr enable -y @pki/10.6

#useradd dirsrv -d /usr/share/dirsrv
#useradd pkiuser -d /usr/share/pki

# source control
yum install -y \
        git \
        git-svn

# build tools
yum install -y --nogpgcheck \
        rpm-build \
        rpmlint \
        make \
        cmake \
        389-ds-base \
        tomcatjss

#        gcc-c++ \
#        fedpkg \
#        fedora-packager \

# C dependencies
#yum install -y --nogpgcheck \
#        apr-util-devel httpd-devel pcre-devel svrcore-devel zlib-devel

dnf builddep -y --spec ../../pki/specs/pki-core.spec.in
dnf builddep -y --spec ../../pki/specs/pki-console.spec.in
dnf builddep -y --spec ../../pki/specs/dogtag-pki-theme.spec.in
dnf builddep -y --spec ../../pki/specs/dogtag-pki.spec.in

#yum-builddep -y --spec ../../pki/specs/pki-core.spec.in
#yum-builddep -y --spec ../../pki/specs/pki-console.spec.in
