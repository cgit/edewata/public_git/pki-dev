#!/bin/sh -x

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-add tpsagent --fullName "TPS Agent"
pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-group-member-add "TPS Agents" tpsagent

REQUEST_ID=`pki -c Secret.123 client-cert-request uid=tpsagent | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret.123 -n caadmin tps-user-cert-add tpsagent --serial $CERT_ID
pki -c Secret.123 client-cert-import tpsagent --serial $CERT_ID

pki -c Secret.123 client-cert-show tpsagent --pkcs12 tpsagent.p12 --pkcs12-password Secret.123
