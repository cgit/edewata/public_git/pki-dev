#!/bin/python

import getopt
import sys

import pki
import pki.client
import pki.crypto
import pki.key
import pki.kra
import pki.systemcert

def usage():
    print "usage: vault-server-remove --user-id <user ID> --secret-id <secret ID>"

def main(argv):

    try:
        opts, _ = getopt.getopt(argv[1:], 'c:d:hv', [
            'user-id=', 'secret-id=',
            'verbose', 'help'])

    except getopt.GetoptError as e:
        print 'ERROR: ' + str(e)
        usage()
        sys.exit(1)

    nssdb_directory = "/root/.dogtag/pki-tomcat/ca/alias"
    nssdb_password = "Secret123"

    transport_cert_nickname = "KRA Transport Certificate"
    admin_cert = "/root/.dogtag/pki-tomcat/ca_admin_cert.pem"

    scheme = 'https'
    host = 'localhost'
    port = '8443'
    subsystem = 'kra'

    user_id = None
    secret_id = None

    for o, a in opts:
        if o == '-d':
            nssdb_directory = a

        elif o == '-c':
            nssdb_password = a

        elif o == '--user-id':
            user_id = a

        elif o == '--secret-id':
            secret_id = a

    if user_id is None or secret_id is None:
        usage()
        sys.exit(1)

    client_key_id = '%s:%s' % (user_id, secret_id)

    crypto = pki.crypto.NSSCryptoProvider(nssdb_directory, nssdb_password)
    crypto.initialize()

    conn = pki.client.PKIConnection(scheme, host, port, subsystem)
    conn.set_authentication_cert(admin_cert)

    kra_client = pki.kra.KRAClient(conn, crypto, transport_cert_nickname)
    key_client = kra_client.keys

    key_info = key_client.get_active_key_info(client_key_id)
    key_id = key_info.get_key_id()

    key_client.modify_key_status(key_id, pki.key.KeyClient.KEY_STATUS_INACTIVE)

if __name__ == '__main__':
    main(sys.argv)
