#!/bin/sh -x

INSTANCE=pki-tomcat
NSSDB_DIR=/etc/dirsrv/slapd-$INSTANCE

certutil -F -d $NSSDB_DIR -f $NSSDB_DIR/password.txt -n "DS Certificate"
