#!/bin/sh -x

cd ~/Downloads

BASE_URL=http://kojipkgs.fedoraproject.org/packages
PACKAGE=pki-tps
VERSION=10.1.0
RELEASE=1
OS=fc20

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/pki-tps-$VERSION-$RELEASE.$OS.x86_64.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/pki-tps-debuginfo-$VERSION-$RELEASE.$OS.x86_64.rpm
