#!/bin/sh

# generate level 1 CA certificate

rm -rf nssdb
mkdir nssdb
echo Secret123 > nssdb/password.txt
certutil -N -d nssdb -f nssdb/password.txt
openssl rand -out nssdb/noise.bin 2048

echo -e "y\n\ny\n" | \
 certutil -S \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -n "Level 1 CA" \
 -s "CN=CA Signing Certificate,O=LEVEL1" \
 -x \
 -t "CTu,Cu,Cu" \
 -m $RANDOM\
 -2 \
 --keyUsage certSigning \
 --nsCertType sslCA,smimeCA,objectSigningCA

certutil -L -d nssdb -n "Level 1 CA" -a > level1.crt

# generate level 2 CA certificate

echo -e "y\n\ny\n" | \
 certutil -R \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -s "CN=CA Signing Certificate,O=LEVEL2" \
 -2 \
 --keyUsage digitalSignature,nonRepudiation,certSigning,crlSigning,critical \
 -o level2.csr.der

BtoA level2.csr.der level2.csr.pem
echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > level2.csr
cat level2.csr.pem >> level2.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> level2.csr
rm level2.csr.der
rm level2.csr.pem

echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a \
 -i level2.csr \
 -o level2.crt \
 -c "Level 1 CA" \
 -1 -2

certutil -A -d nssdb -n "Level 2 CA" -i level2.crt -t "CTu,Cu,Cu"

openssl crl2pkcs7 -nocrl -certfile level1.crt -certfile level2.crt -out cert_chain.p7b

# sign the CA signing certificate

echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a -i ca_signing.csr \
 -o ca_signing.crt \
 -c "Level 2 CA" \
 -1 -2
