#!/bin/sh +x

certutil -R \
 -d nssdb \
 -z nssdb/noise.bin \
 -f nssdb/password.txt \
 -s "UID=testuser,O=EXAMPLE" \
 -o nssdb/testuser.csr.der

BtoA nssdb/testuser.csr.der nssdb/testuser.csr.pem
echo "-----BEGIN NEW CERTIFICATE REQUEST-----" > nssdb/testuser.csr
cat nssdb/testuser.csr.pem >> nssdb/testuser.csr
echo "-----END NEW CERTIFICATE REQUEST-----" >> nssdb/testuser.csr
rm nssdb/testuser.csr.der
rm nssdb/testuser.csr.pem
