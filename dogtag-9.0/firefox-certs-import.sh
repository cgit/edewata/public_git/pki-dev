#!/bin/sh -x

. ./ca-include.sh

FIREFOX_DIR=~/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

pk12util -i /var/lib/pki-ca/certs/ca-client-certs.p12 -d $FIREFOX_DIR/$PROFILE -W Secret123
certutil -M -n caadmin -t u,u,u -d $FIREFOX_DIR/$PROFILE

pk12util -i /var/lib/pki-kra/certs/kra-client-certs.p12 -d $FIREFOX_DIR/$PROFILE -W Secret123
certutil -M -n kraadmin -t u,u,u -d $FIREFOX_DIR/$PROFILE
