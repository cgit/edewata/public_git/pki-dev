#!/bin/sh -x

. ./ca-include.sh

pkiremove -pki_instance_root=$INSTANCE_ROOT	\
	-pki_instance_name=$CA_INSTANCE_NAME	\
	-force
