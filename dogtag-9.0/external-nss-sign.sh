#!/bin/sh

#pki ca-cert-request-profile-show caCACert --output caCACert.xml
#pki cert-request-submit caCACert.xml

#pki -d ~/.dogtag/pki-tomcat/ca/alias/ -c Secret123 -n caadmin ca-cert-request-review --action approve 10

#pki cert-show --output ca.crt 0x7
#pki cert-show --output external.crt 0x1

rm -rf nssdb
mkdir nssdb
echo Secret123 > nssdb/password.txt
certutil -N -d nssdb -f nssdb/password.txt

openssl rand -out nssdb/noise.bin 2048
echo -e "y\n\ny\n" | \
 certutil -S \
 -d nssdb \
 -f nssdb/password.txt \
 -z nssdb/noise.bin \
 -n "External CA" \
 -s "CN=CA Signing Certificate,O=EXAMPLE" \
 -x \
 -t "CTu,CTu,CTu" \
 -m 1\
 -2 \
 --keyUsage certSigning \
 --nsCertType sslCA,smimeCA,objectSigningCA

certutil -L -d nssdb -n "External CA" -a > ./external.crt

echo -e "0\n1\n5\n6\n9\ny\ny\n\ny\n" | \
 certutil -C \
 -d nssdb \
 -f nssdb/password.txt \
 -m $RANDOM \
 -a -i ca_signing.csr \
 -o ca_signing.crt \
 -c "External CA" \
 -1 -2

#certutil -C \
# -d nssdb \
# -f nssdb/password.txt \
# -m $RANDOM \
# -a -i ca_signing.csr \
# -o ca_signing.crt \
# -c "External CA"
