#!/bin/sh -x

rpm -e --nodeps pki-native-tools
rpm -e --nodeps pki-symkey
rpm -e --nodeps pki-ca
rpm -e --nodeps ipa-pki-ca-theme
rpm -e --nodeps pki-util
rpm -e --nodeps pki-java-tools
rpm -e --nodeps ipa-pki-common-theme
rpm -e --nodeps pki-setup
rpm -e --nodeps pki-common
rpm -e --nodeps pki-silent
rpm -e --nodeps pki-java-tools-javadoc
rpm -e --nodeps pki-core-debuginfo
rpm -e --nodeps pki-selinux
rpm -e --nodeps pki-common-javadoc
rpm -e --nodeps pki-util-javadoc
