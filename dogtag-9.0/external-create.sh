#!/bin/sh -x

mkdir -p build

rm -f /tmp/ca_signing.csr
rm -r /tmp/external.crt
rm -r /tmp/cert_chain.p7b
rm -f /tmp/ca_signing.crt

pkicreate \
 -pki_instance_root=/var/lib \
 -pki_instance_name=pki-ca \
 -subsystem_type=ca \
 -secure_port=9443 \
 -unsecure_port=9180 \
 -tomcat_server_port=9701 \
 -user=pkiuser \
 -group=pkiuser \
 -redirect conf=/etc/pki-ca \
 -redirect logs=/var/log/pki-ca | tee build/external-create.log
