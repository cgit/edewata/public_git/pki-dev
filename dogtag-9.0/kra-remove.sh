#!/bin/sh -x

INSTANCE_NAME=pki-kra

pkiremove -pki_instance_root=/var/lib\
	-pki_instance_name=$INSTANCE_NAME\
	-force
