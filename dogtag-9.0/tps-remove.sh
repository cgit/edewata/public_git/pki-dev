#!/bin/sh -x

. ./tps-include.sh

pkiremove -pki_instance_root=$INSTANCE_ROOT     \
          -pki_instance_name=$TPS_INSTANCE_NAME \
          -force                                \
          -verbose
