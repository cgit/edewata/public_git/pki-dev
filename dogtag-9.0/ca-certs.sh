#!/bin/sh

certutil -L -d /var/lib/pki-ca/alias

certutil -L -d /var/lib/pki-ca/certs
