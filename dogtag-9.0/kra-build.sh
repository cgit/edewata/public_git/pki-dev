#!/bin/sh -x

WORK_DIR=`pwd`
PROJECT_DIR=`cd ../.. ; pwd`
COMPONENT=kra

mkdir -p $WORK_DIR/build
rm -rf $WORK_DIR/build/$COMPONENT

cd $PROJECT_DIR
rm -rf packages
mkdir -p packages

pki/scripts/compose_pki_${COMPONENT}_packages hybrid_rpms 2>&1 | tee packages/build.log

mv packages $WORK_DIR/build/$COMPONENT
cd $WORK_DIR/build/$COMPONENT

mkdir -p repo
mv `find RPMS -name *.rpm` repo
createrepo repo
