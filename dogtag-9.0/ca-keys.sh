#!/bin/sh

NSSDB_PASSWORD=`grep internal= /var/lib/pki-ca/conf/password.conf | awk -F = '{ print $2; }'`

echo $NSSDB_PASSWORD > /var/lib/pki-ca/alias/password.txt

certutil -K -d /var/lib/pki-ca/alias -f /var/lib/pki-ca/alias/password.txt

#certutil -K -d /var/lib/pki-ca/certs -f /var/lib/pki-ca/certs/password.txt
