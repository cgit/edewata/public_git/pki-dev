#!/bin/sh

PROFILE=caCACert
#PROFILE=caInstallCACert

REQUEST_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-cert-request-submit --profile $PROFILE --csr-file ca_signing.csr | grep "Request ID:" | awk -F ': ' '{print $2;}'`
echo Request ID: $REQUEST_ID

CERT_ID=`pki -d ~/.dogtag/pki-tomcat/ca/alias -c Secret123 -n caadmin ca-cert-request-review --action approve $REQUEST_ID | grep "Certificate ID:" | awk -F ': ' '{print $2;}'`
echo Certificate ID: $CERT_ID

pki cert-show --output ca_signing.crt $CERT_ID

pki cert-show --output external.crt 0x1
