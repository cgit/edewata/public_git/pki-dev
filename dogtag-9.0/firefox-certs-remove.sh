#!/bin/sh -x

. ./ca-include.sh

FIREFOX_DIR=~/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

cd $FIREFOX_DIR/$PROFILE

certutil -D -n "$CA_ADMIN_NAME" -d .
certutil -D -n "kraadmin" -d .
certutil -D -n "$CA_SUBSYSTEM_NAME - $REALM" -d .
certutil -D -n "$HOSTNAME" -d .
certutil -D -n "$HOSTNAME #2" -d .
certutil -D -n "$HOSTNAME #3" -d .
