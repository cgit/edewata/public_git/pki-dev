#!/bin/sh

certutil -O -d /var/lib/pki-ca/alias -n "ocspSigningCert cert-pki-ca"
certutil -O -d /var/lib/pki-ca/certs -n "caadmin"
