#!/bin/sh -x

WORK_DIR=`pwd`
PROJECT_DIR=`cd ../.. ; pwd`
COMPONENT=theme

mkdir -p $WORK_DIR/build
rm -rf $WORK_DIR/build/$COMPONENT

cd $PROJECT_DIR
rm -rf packages
mkdir -p packages

pki/scripts/compose_dogtag_pki_theme_packages rpms | tee packages/build.log

mv packages $WORK_DIR/build/$COMPONENT
cd $WORK_DIR/build/$COMPONENT

mkdir -p repo
mv `find RPMS -name *.rpm` repo
createrepo repo
