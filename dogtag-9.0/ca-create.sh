#!/bin/sh -x

. ./ca-include.sh

pkicreate \
 -pki_instance_root=$INSTANCE_ROOT \
 -pki_instance_name=$CA_INSTANCE_NAME \
 -subsystem_type=$CA_SUBSYSTEM_TYPE \
 -secure_port=$CA_SECURE_PORT 	\
 -unsecure_port=$CA_UNSECURE_PORT \
 -tomcat_server_port=$CA_TOMCAT_SERVER_PORT \
 -user=$INSTANCE_USER \
 -group=$INSTANCE_GROUP \
 -redirect conf=/etc/$CA_INSTANCE_NAME \
 -redirect logs=/var/log/$CA_INSTANCE_NAME \
 -verbose

#cd $INSTANCE_ROOT/$CA_INSTANCE_NAME

#ln -s /usr/share/tomcat6/bin bin
#ln -s /usr/share/tomcat6/lib lib
#rm -f webapps/ca/WEB-INF/lib/pki-*

#rm -rf webapps/ca/WEB-INF/classes
#ln -s $SRC_DIR/pki/build/classes webapps/ca/WEB-INF

#systemctl restart pki-cad@$CA_INSTANCE_NAME.service
#/sbin/service pki-cad restart pki-ca
