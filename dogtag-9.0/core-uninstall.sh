#!/bin/sh -x

yum erase -y\
	pki-common\
	pki-silent\
	pki-core-debuginfo\
	pki-setup\
	pki-java-tools-javadoc\
	pki-java-tools\
	pki-util\
	pki-common-javadoc\
	pki-selinux\
	pki-util-javadoc\
	pki-symkey\
	pki-native-tools\
	pki-ca
