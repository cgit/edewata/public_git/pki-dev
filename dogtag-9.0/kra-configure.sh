#!/bin/sh -x

PKI_DEV_SRC=`cd .. ; pwd`

INSTANCE_NAME=pki-kra
PASSWORD=Secret123
PIN=`grep preop.pin= /var/lib/$INSTANCE_NAME/conf/CS.cfg | awk -F= '{ print $2; }'`

REALM=EXAMPLE-COM
CERTS=/var/lib/$INSTANCE_NAME/certs
rm -rf $CERTS
mkdir -p $CERTS

pkisilent ConfigureDRM \
        -cs_hostname "$HOSTNAME" \
        -cs_port 10443 \
        -preop_pin "$PIN" \
        -client_certdb_dir "$CERTS" \
        -client_certdb_pwd "$PASSWORD" \
        -token_name "internal" \
        -sd_hostname "$HOSTNAME" \
        -sd_admin_port 9443 \
        -sd_ssl_port 9443 \
        -sd_agent_port 9443 \
        -sd_admin_name "caadmin" \
        -sd_admin_password "$PASSWORD" \
        -domain_name "$REALM" \
        -subsystem_name "Data Recovery Manager" \
        -ldap_host "localhost" \
        -ldap_port "389" \
        -base_dn "ou=kra,dc=example,dc=com" \
        -db_name "example.com-$INSTANCE_NAME" \
        -bind_dn "cn=Directory Manager" \
        -bind_password "$PASSWORD" \
        -remove_data true \
        -key_type rsa \
        -key_size 2048 \
        -signing_algorithm SHA256withRSA \
        -drm_transport_cert_subject_name "CN=DRM Transport Certificate,O=$REALM" \
        -drm_storage_cert_subject_name "CN=DRM Storage Certificate,O=$REALM" \
        -drm_server_cert_subject_name "CN=$HOSTNAME,O=$REALM" \
        -drm_subsystem_cert_subject_name "CN=DRM Subsystem Certificate,O=$REALM" \
        -drm_audit_signing_cert_subject_name "CN=DRM Audit Signing Certificate,O=$REALM" \
        -ca_hostname "$HOSTNAME" \
        -ca_port 9180 \
        -ca_ssl_port 9443 \
        -backup_fname "$CERTS/kra-server-certs.p12" \
        -backup_pwd "$PASSWORD" \
        -admin_user "kraadmin" \
        -agent_name "kraadmin" \
        -admin_email "kraadmin@example.com" \
        -admin_password "$PASSWORD" \
        -agent_key_size 2048 \
        -agent_key_type rsa \
        -agent_cert_subject "CN=kraadmin,UID=kraadmin,E=kraadmin@example.com,O=$REALM"

echo $PASSWORD > "$CERTS/password.txt"
PKCS12Export -d "$CERTS" -o "$CERTS/kra-client-certs.p12" -p "$CERTS/password.txt" -w "$CERTS/password.txt"

systemctl restart pki-krad@$INSTANCE_NAME.service
