#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

cd $SRC_DIR

rm -rf packages
mkdir -p packages

pki/scripts/compose_pki_core_packages hybrid_rpms

mkdir -p repo
mv `find packages/RPMS -name *.rpm` repo
createrepo repo

rm -rf packages.core
mv packages packages.core
