#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

cd $SRC_DIR/pki

mkdir -p build
cd build

cmake\
 -DCMAKE_VERBOSE_MAKEFILE=ON\
 -DCMAKE_INSTALL_PREFIX:PATH=/usr\
 -DINCLUDE_INSTALL_DIR:PATH=/usr/include\
 -DLIB_INSTALL_DIR:PATH=/usr/lib64\
 -DSYSCONF_INSTALL_DIR:PATH=/etc\
 -DSHARE_INSTALL_PREFIX:PATH=/usr/share\
 -DLIB_SUFFIX=64\
 -DBUILD_SHARED_LIBS:BOOL=ON\
 -DVAR_INSTALL_DIR:PATH=/var\
 -DBUILD_PKI_CONSOLE:BOOL=ON\
 -DWITH_JAVADOC=OFF\
 -DJAVA_LIB_INSTALL_DIR=/usr/lib64/java ..

make all install
