#!/bin/sh -x

mkdir -p build

PIN=`grep preop.pin= /var/lib/pki-ca/conf/CS.cfg | awk -F= '{ print $2; }'`
NSSDB_PASSWORD=`grep internal= /var/lib/pki-ca/conf/password.conf | awk -F = '{ print $2; }'`
echo $NSSDB_PASSWORD > /var/lib/pki-ca/alias/password.txt

CERTS=/var/lib/pki-ca/certs
rm -rf $CERTS
mkdir -p $CERTS
echo Secret123 > /var/lib/pki-ca/certs/password.txt

pkisilent ConfigureCA \
 -cs_hostname $HOSTNAME \
 -cs_port 9443 \
 -preop_pin $PIN \
 -client_certdb_dir /var/lib/pki-ca/certs \
 -client_certdb_pwd Secret123 \
 -token_name internal \
 -domain_name EXAMPLE-COM \
 -subsystem_name 'Certificate Authority' \
 -ldap_host $HOSTNAME \
 -ldap_port 389 \
 -base_dn ou=ca,dc=example,dc=com \
 -db_name example.com-pki-ca \
 -bind_dn 'cn=Directory Manager' \
 -bind_password Secret123 \
 -remove_data true \
 -key_type rsa \
 -key_size 2048 \
 -key_algorithm SHA256withRSA \
 -signing_signingalgorithm SHA256withRSA \
 -save_p12 true \
 -backup_fname /var/lib/pki-ca/certs/ca-server-certs.p12 \
 -backup_pwd Secret123 \
 -ca_sign_cert_subject_name 'CN=Certificate Authority,O=EXAMPLE-COM' \
 -ca_ocsp_cert_subject_name 'CN=OCSP Signing Certificate,O=EXAMPLE-COM' \
 -ca_server_cert_subject_name CN=$HOSTNAME,O=EXAMPLE-COM \
 -ca_subsystem_cert_subject_name 'CN=CA Subsystem Certificate,O=EXAMPLE-COM' \
 -ca_audit_signing_cert_subject_name 'CN=CA Audit Signing Certificate,O=EXAMPLE-COM' \
 -admin_user caadmin \
 -agent_name caadmin \
 -admin_email caadmin@example.com \
 -admin_password Secret123 \
 -agent_key_size 2048 \
 -agent_key_type rsa \
 -agent_cert_subject CN=caadmin,UID=caadmin,E=caadmin@example.com,O=EXAMPLE-COM \
 -external true \
 -ext_csr_file /tmp/ca_signing.csr | tee build/external-step1.log

/bin/cp -f /tmp/ca_signing.csr .

