pkisilent ConfigureCA \
    -cs_hostname `hostname` \
    -cs_port 9445 \
    -client_certdb_dir /tmp/tmp-DyO1lT \
    -client_certdb_pwd Secret123 \
    -preop_pin Secret123 \
    -domain_name IPA \
    -admin_user admin \
    -admin_email root@localhost \
    -admin_password Secret123 \
    -agent_name ipa-ca-agent \
    -agent_key_size 2048 \
    -agent_key_type rsa \
    -agent_cert_subject CN=ipa-ca-agent,O=EXAMPLE.COM \
    -ldap_host `localhost` \
    -ldap_port 7389 \
    -bind_dn "cn=Directory Manager" \
    -bind_password Secret123 \
    -base_dn o=ipaca \
    -db_name ipaca \
    -key_size 2048 \
    -key_type rsa \
    -key_algorithm SHA256withRSA \
    -save_p12 true \
    -backup_pwd Secret123 \
    -subsystem_name pki-cad \
    -token_name internal \
    -ca_subsystem_cert_subject_name "CN=CA Subsystem,O=EXAMPLE.COM" \
    -ca_subsystem_cert_subject_name "CN=CA Subsystem,O=EXAMPLE.COM" \
    -ca_ocsp_cert_subject_name "CN=OCSP Subsystem,O=EXAMPLE.COM" \
    -ca_server_cert_subject_name CN=`hostname`,O=EXAMPLE.COM \
    -ca_audit_signing_cert_subject_name "CN=CA Audit,O=EXAMPLE.COM" \
    -ca_sign_cert_subject_name "CN=Certificate Authority,O=EXAMPLE.COM" \
    -external false \
    -clone false
