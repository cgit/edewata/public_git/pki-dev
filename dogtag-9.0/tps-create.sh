#!/bin/sh -x

. ./tps-include.sh

pkicreate -pki_instance_root=$INSTANCE_ROOT             \
             -pki_instance_name=$TPS_INSTANCE_NAME      \
             -subsystem_type=$TPS_SUBSYSTEM_TYPE        \
             -secure_port=$TPS_SECURE_PORT              \
             -non_clientauth_secure_port=$TPS_NON_CLIENTAUTH_SECURE_PORT \
             -unsecure_port=$TPS_UNSECURE_PORT          \
             -user=$INSTANCE_USER                       \
             -group=$INSTANCE_GROUP                     \
             -redirect conf=/etc/$TPS_INSTANCE_NAME     \
             -redirect logs=/var/log/$TPS_INSTANCE_NAME \
             -verbose
