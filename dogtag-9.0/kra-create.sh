#!/bin/sh -x

SRC_DIR=`cd ../.. ; pwd`

INSTANCE_NAME=pki-kra

pkicreate -pki_instance_root=/var/lib			\
          -pki_instance_name=$INSTANCE_NAME		\
          -subsystem_type=kra				\
          -secure_port=10443				\
          -unsecure_port=10180				\
          -tomcat_server_port=10701			\
          -user=pkiuser					\
          -group=pkiuser				\
          -audit_group=pkiaudit				\
          -redirect conf=/etc/$INSTANCE_NAME		\
          -redirect logs=/var/log/$INSTANCE_NAME	\
          -verbose

#cd /var/lib/$INSTANCE_NAME

#ln -s /usr/share/tomcat6/bin bin
#ln -s /usr/share/tomcat6/lib lib
#rm -f webapps/kra/WEB-INF/lib/pki-*

#rm -rf webapps/kra/WEB-INF/classes
#ln -s $SRC_DIR/pki/build/classes webapps/kra/WEB-INF

#systemctl restart pki-krad@$INSTANCE_NAME.service
/sbin/service pki-krad restart pki-kra
