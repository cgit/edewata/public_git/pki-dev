#!/bin/sh

grep "internal=" /var/lib/pki-ca/conf/password.conf | awk -F= '{print $2}' > internal.txt

PKCS12Export -debug \
 -d /var/lib/pki-ca/alias \
 -p internal.txt \
 -o ../scripts/ca_backup_keys.p12 \
 -w password.txt
