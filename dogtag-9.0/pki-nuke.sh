#!/bin/sh

SUBSYSTEM_NAME=$1

if [ "$SUBSYSTEM_NAME" == "" ]; then
    echo "usage: pki-nuke.sh <instance name>"
    exit 1
fi

echo "Deleting subsystem $SUBSYSTEM_NAME"

pids="`ps -ef | grep catalina.base=/var/lib/pki-$SUBSYSTEM_NAME | grep -v grep | awk '{print $2}'`"
for pid in $pids; do
    kill -9 $pid
done

rm -rf /etc/pki-$SUBSYSTEM_NAME
rm -rf /etc/sysconfig/pki-$SUBSYSTEM_NAME
rm -rf /etc/sysconfig/pki/ca/pki-$SUBSYSTEM_NAME
rm -rf /var/lib/pki-$SUBSYSTEM_NAME
rm -rf /var/log/pki-$SUBSYSTEM_NAME
rm -rf /var/log/pki-*.log
#rm -rf /var/lib/ipa/pki-$SUBSYSTEM_NAME
