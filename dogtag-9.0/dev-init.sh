#!/bin/sh -x

useradd dirsrv -d /usr/share/dirsrv
useradd pkiuser -d /usr/share/pki

# development
yum install -y\
        git

yum install -y --skip-broken\
        gcc-c++\
        jakarta-commons-io\
        jakarta-commons-lang\
        cmake\
        java-1.6.0-openjdk-devel\
        jpackage-utils\
        jss\
        ldapjdk\
        nspr-devel\
        nss-devel\
        openldap-devel\
        osutil\
        tomcatjss\
        velocity\
        xalan-j2\
        xerces-j2

exit

# build
yum-builddep --skip-broken ../../pki/specs/pki-core.spec
yum install -y --skip-broken\
        gcc-c++\
        rpm-build\
        createrepo

# test
yum install -y --skip-broken\
        389-ds-base

# obsolete
#yum install -y\
#        kdiff3\
#        cmake\
#        gcc\
#        mock\
#        java-devel\
#        ldapjdk\
#        idm-console-framework\
#        apache-commons-cli\
#        apache-commons-codec\
#        httpcomponents-client\
#        httpcomponents-core\
#        nspr-devel\
#        nss-devel\
#        openldap-clients\
#        openldap-devel\
#        openssl-devel\
#        velocity\
#        xalan-j2\
#        xerces-j2\
#        selinux-policy-devel\
#        389-ds-base\
#        java-1.7.0-openjdk\
#        junit\
#        jss\
#        javassist\
#        jettison\
#        resteasy\
#        tomcatjss\
#        perl-File-Slurp\
#        perl-XML-LibXML\
#        perl-Crypt-SSLeay\
#        pylint\
#        python-ldap\
#        python-lxml\
#        python-nss\
#        python-sphinx\
#        freeipa-python\
#        eclipse-jdt\
#        fedora-packager\
#        apr-devel\
#        apr-util-devel\
#        httpd-devel\
#        pcre-devel\
#        dnf\
#        dnf-plugins-core\
#        @development-tools
