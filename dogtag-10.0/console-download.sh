#!/bin/sh -x

mkdir -p ~/Downloads
cd ~/Downloads

BASE_URL=http://kojipkgs.fedoraproject.org/packages
PACKAGE=pki-console
VERSION=10.0.7
RELEASE=1
OS=fc19

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-console-$VERSION-$RELEASE.$OS.noarch.rpm
