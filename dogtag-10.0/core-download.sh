#!/bin/sh -x

mkdir -p ~/Downloads
cd ~/Downloads

BASE_URL=http://kojipkgs.fedoraproject.org/packages
PACKAGE=pki-core
VERSION=10.0.7
RELEASE=1
OS=fc19

wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-base-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-ca-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-javadoc-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-kra-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-ocsp-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-server-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-tks-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/noarch/pki-tps-tomcat-$VERSION-$RELEASE.$OS.noarch.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/pki-symkey-$VERSION-$RELEASE.$OS.x86_64.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/pki-tools-$VERSION-$RELEASE.$OS.x86_64.rpm
wget $BASE_URL/$PACKAGE/$VERSION/$RELEASE.$OS/x86_64/pki-core-debuginfo-$VERSION-$RELEASE.$OS.x86_64.rpm
