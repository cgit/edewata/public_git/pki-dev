#!/bin/sh -x

WORK_DIR=`pwd`
PROJECT_DIR=`cd ../.. ; pwd`
COMPONENT=core

cd $WORK_DIR/build/$COMPONENT/repo

yum localinstall -y *.rpm
